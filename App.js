import { StyleSheet, Text, View } from 'react-native';
import {Block} from './src/components';
import Navigation from './src/navigation';
import React from "react";
import {store} from "./src/store/index";
import {Provider} from "react-redux";

export default function App() {
  return (
    <Provider store={store}>
      <Block>
        <Navigation/>
      </Block>
    </Provider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
