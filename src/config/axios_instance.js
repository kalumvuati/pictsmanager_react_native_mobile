import {API_URL} from '@env';
import axios from 'axios';

export const axiosInstance = axios.create({
    baseURL: API_URL,
    headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
    },
    withCredentials: true,
});

export const SERVER_URI = API_URL;

export const post = async (url, data, options = {}) => {
    try {
        return await axiosInstance.post(url, data, options);
    } catch (error) {
        return error.response.data;
    }
}

export const put = async (url, data, options = {}) => {
    try {
        console.log("put called");
        return await axiosInstance.put(url, data, options);
    } catch (error) {
        return error.response.data;
    }
}


/*

axios.delete(URL, {
  headers: {
    Authorization: authorizationToken
  },
  data: {
    source: source
  }
});

 */
export const remove = async (url, options = {}) => {
    try {
        return await axiosInstance.delete(url, options);
    } catch (error) {
        return error.response.data;
    }
}

export const get = async (url, data, options = {}) => {
    try {
        return await axiosInstance.get(url, options);
    } catch (error) {
        return error.response.data;
    }
}
