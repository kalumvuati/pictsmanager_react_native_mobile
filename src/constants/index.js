import  * as theme from './theme';
import * as mocks from './mocks';
import {API_URL} from '@env';

export {
    theme,
    mocks,
}