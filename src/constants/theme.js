const colors = {
    accent: "#F3534A",
    primary: "#1266F1",
    secondary: "#B23CFD",
    tertiary: "#FFE358",
    success: "#00B74A",
    warning: "#FFA900",
    danger: "#F93154",
    info: "#39C0ED",
    dark: "#262626",
    black: "#323643",
    light: "#FBFBFB",
    white: "#FFFFFF",
    whiteAlpha: (opacity) => `rgba(255, 255, 255, ${opacity})`,
    gray: "#9DA3B4",
    gray2: "#C5CCD6",
    gray3: "#9FA6B2",
    gray4: "#ECECEC",
    default: '#512DA8',
    defaultRGBA: (opacity) => `rgba(81, 45, 168, ${opacity})`,
    transparent: "transparent",
    dark_white: "#F4F5F7",
};

const sizes = {
    // global sizes
    base: 16,
    font: 14,
    radius: 6,
    padding: 25,

    // font sizes
    h1: 26,
    h2: 20,
    h3: 18,
    title: 18,
    header: 16,
    body: 14,
    caption: 12,

    // Button
    btn: 20,
    btnWith: '80%',
};

const fonts = {
    h1: {
        fontSize: sizes.h1
    },
    h2: {
        fontSize: sizes.h2
    },
    h3: {
        fontSize: sizes.h3
    },
    header: {
        fontSize: sizes.header
    },
    title: {
        fontSize: sizes.title
    },
    body: {
        fontSize: sizes.body
    },
    caption: {
        fontSize: sizes.caption
    }
};

export { colors, sizes, fonts };