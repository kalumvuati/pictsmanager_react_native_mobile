const categories = [
    {
        id: 1,
        name: 'Plants',
        tags: [
            'products',
            'inspiration',
            'nature',
        ],
        count: 154,
       // image: 'https://images.unsplash.com/photo-1518791841217-8f162f1e1131?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60',
    },
    {
        id: 2,
        name: 'Animals',
        tags: [
            'products',
            'inspiration',
            'nature',
        ],
        count: 154,
        //image: 'https://images.unsplash.com/photo-1518791841217-8f162f1e1131?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60',
    },
    {
        id: 3,
        name: 'Food',
        tags: [
            'products',
            'shop',
        ],
        count: 154,
        //image: 'https://images.unsplash.com/photo-1518791841217-8f162f1e1131?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60',
    },
    {
        id: 4,
        name: 'Fashion',
        tags: [
            'products',
            'shop',
        ],
        count: 154,
        //image: 'https://images.unsplash.com/photo-1518791841217-8f162f1e1131?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60',
    }
];

const products = [
    {
        id: 1,
        name: 'Plants',
        description: 'lorem ipsum dolor sit amet consectetur adipisicing elit. Quisquam, quidem.',
        tags : [
            'Interior',
            '27m^2',
            'Ideas',
        ],
        gallery: [
            // require('../assets/images/product/product1.jpg'),
            // require('../assets/images/product/product2.jpg'),
            // require('../assets/images/product/product3.jpg'),

            // require('../assets/images/product/product4.jpg'),
        ]
    }
];

const explore = [
    // Images - nature
    // require('../assets/images/nature/nature1.jpg'),
    // require('../assets/images/nature/nature2.jpg'),
    // require('../assets/images/nature/nature3.jpg'),
    // require('../assets/images/nature/nature4.jpg'),
    // require('../assets/images/nature/nature5.jpg'),
    // require('../assets/images/nature/nature6.jpg'),

    // Image - animal
    // require('../assets/images/animal/animal1.jpg'),
    // require('../assets/images/animal/animal2.jpg'),
    // require('../assets/images/animal/animal3.jpg'),
];

const profile = {
    username: 'John Doe',
    location: 'New York, USA',
    email: 'johndoe@tutofree.fr',
    //avatar: require('../assets/images/avatar/avatar1.jpg'),
    budget: 2000,
    monthly_cap: 5000,
    notifications: true,
    newsletter: false,
};


export  {
    categories,
    products,
    profile,
    explore
}