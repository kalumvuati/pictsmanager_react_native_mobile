import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Gallery from './Gallery';
import Settings from './Settings';
import Favorite from './Favorite';
import Profile from './Profile';
import {MaterialCommunityIcons} from '@expo/vector-icons';
import {theme} from '../constants';
import {Dimensions, StyleSheet,  View} from 'react-native';

const Tab = createBottomTabNavigator();

export function Main(props) {

    const tabs = [
        {
            name: 'Home',
            component: Gallery,
            options: {
                tabBarIcon: ({color, size, focused}) => (
                    <View style={focused ? styles.container.containerFocused : styles.container.container}>
                        <MaterialCommunityIcons
                            name="image"
                            color={focused ? theme.colors.white :  theme.colors.gray}
                            size={size}
                            style={focused ? styles.container.iconFocused :  styles.container.icon}
                        />
                    </View>
                ),
            },
        },
        {
            name: 'Favorite',
            component: Favorite,
            options: {
                tabBarIcon: ({color, size, focused}) => (
                    <View style={focused ? styles.container.containerFocused : styles.container.container}>
                        <MaterialCommunityIcons
                            name="heart"
                            color={focused ? theme.colors.white :  theme.colors.gray}
                            size={size}
                            style={focused ? styles.container.iconFocused :  styles.container.icon}
                        />
                    </View>
                )
            }
        },
        {
            name: 'Profile',
            component: Profile,
            options: {
                tabBarIcon: ({color, size, focused}) => (
                    <View style={focused ? styles.container.containerFocused : styles.container.container}>
                        <MaterialCommunityIcons
                            name="account"
                            color={focused ? theme.colors.white :  theme.colors.gray}
                            size={size}
                            style={focused ? styles.container.iconFocused :  styles.container.icon}
                        />
                    </View>
                )
            }
        },
        {
            name: 'Settings',
            component: Settings,
            options: {
                tabBarIcon: ({color, size, focused}) => (
                    <View style={focused ? styles.container.containerFocused : styles.container.container}>
                        <MaterialCommunityIcons
                            name="cog"
                            color={focused ? theme.colors.white :  theme.colors.gray}
                            size={size}
                            style={focused ? styles.container.iconFocused :  styles.container.icon}
                        />
                    </View>
                )
            }
        }
    ];

    return (
        <Tab.Navigator
            initialRouteName="Gallery"
            screenOptions={{
                tabBarShowLabel: false,
                headerShown: false,
                tabBarActiveTintColor: theme.colors.white,
                tabBarStyle: styles.container,
            }}
        >
            {tabs.map((tab, index) => (
                <Tab.Screen
                    key={index}
                    name={tab.name}
                    component={tab.component}
                    options={tab.options}
                    listeners={({navigation}) => ({
                        tabPress: event => {
                            //console.log('tabPress: ', event);
                        },
                    })}
                />
            ))}
        </Tab.Navigator>
    );
}

const width = Dimensions.get('window').width;
const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        width: width - 40,
        height: 60,
        bottom: 15,
        borderRadius: 6,
        alignSelf: 'center',
        marginHorizontal: 20,
        backgroundColor: theme.colors.default,

        shadowColor: theme.colors.white,
        shadowOpacity: 0.06,
        shadowOffset: {
            width: 10,
            height: 10,
        },
        icon: {
            alignItems: 'center',
            justifyContent: 'center',
        },
        iconFocused: {
            alignSelf: 'center',
            alignItems: 'center',
        },
        containerFocused: {
            width: '90%',
            height: '100%',

            alignItems: 'center',
            justifyContent: 'center',

            borderStyle: 'solid',
            borderColor: theme.colors.danger,
            borderTopWidth: 4,

        },
        container: {
            width: '100%',
            height: '100%',

            alignItems: 'center',
            justifyContent: 'center',
        }
    },
});
