import {View, ScrollView, Image, StyleSheet, TouchableOpacity, Dimensions, Pressable} from "react-native";
import React, {Component, useEffect} from 'react';
import {theme} from "../constants";
import {ChatAltIcon, HeartIcon, PencilIcon, ShareIcon, TrashIcon} from "react-native-heroicons/solid";
import {Block, Button, Input, Text} from "../components";
import CommentItems from "../components/CommentItems";
import SelectMulti from "../components/SelectMulti";
import ModalP from "../components/ModalP";
import Select from "../components/Select";
import {useSelector, useDispatch} from "react-redux";
import {getUser} from "../store/features/auth/AuthSlice";
import {isAuthenticated} from "../store/features/auth/AuthSlice";
import {addOrUpdatePicture, deletePicture, getAlbumsSlog, getPictureBy} from '../store/features/album/AlbumSlice';
import {
    createOrUpdatePicture,
    findPictureBy, getUsersByPictureShared,
    removePicture, removeUsersSharedFromPicture,
    sharePicture,
} from '../services/PictureService';
import {axiosInstance} from '../config/axios_instance';
import { getUsersIds} from '../services/UserService';

const Picture = (props) => {

    const { navigation, route, label, error, labelStyle, children } = props;
    const { album_id, picture_id } = route.params;
    const header_image = require("../../assets/images/BG1.jpg");
    const [isLoading, setIsLoading] = React.useState(false);

    // TODO : To look for a better way to do this
    const picture_from_store = useSelector(state => getPictureBy(state, {album_id, picture_id}));
    const albumsList = useSelector(state => getAlbumsSlog(state));
    if (picture_from_store === undefined) {
        return (
            <Block flex={1} center middle>
                <Text h1 center bold>
                    Picture not found
                </Text>
                <TouchableOpacity onPress={() => navigation.goBack()}>
                    <Text h3 center bold primary>
                        Go back
                    </Text>
                </TouchableOpacity>
            </Block>
        );
    }

    const [picture, setPicture] = React.useState({
        ...picture_from_store,
        shared_users: [],
    });
    const dispatch = useDispatch();
    const [isOnEdit, setIsOnEdit] = React.useState(false);
    const [isOnShare, setIsOnShare] = React.useState(false);
    const [isOwner, setIsOwner] = React.useState(false);
    const [usersSelected, setUsersSelected] = React.useState([]);
    const [usersToShare, setUsersToShare] = React.useState([]);
    const [pictureWatcher, setPictureWatcher] = React.useState(false);

    const currentAlbum = albumsList.find(album => album.id === album_id);

    const user = useSelector(getUser);
    const isAuth = useSelector(isAuthenticated);

    // Picture Fields
    const setPictureAlbumId = (AlbumId) => {
        setPicture({
            ...picture,
            AlbumId,
        });
    }
    const setPicturePicture = (picture_p) => {
        setPicture({
            ...picture,
            picture_p
        });
    }
    const setPictureTags = (tag) => {
        setPicture({
            ...picture,
            tags: [...picture.tags, tag]
        });
    }
    const setPictureDescription = (description) => {
        setPicture({
            ...picture,
            description,
        });
    }

    const handlerEditPicture = () => {
        createOrUpdatePicture(picture)
            .then((response) => {
                if (response.status === 201 || response.status === 202) {
                    dispatch(addOrUpdatePicture(response.data.data));
                    setPictureWatcher(true);
                } else {
                    // TODO : to implement useForm Hook
                    console.log("ERROR :  ", response);
                }
            })
            .catch((error) => {
                console.warn(error);
            });
        setIsOnEdit(false);
    }

    const handlerDeletePicture = () => {
        removePicture(picture.id)
            .then((response) => {
                if (response.status === 200) {
                    // TODO : to implement  because is sending :Rendered fewer hooks than expected. This may be caused
                    //  by an accidental early return statement. (ERROR)
                    // dispatch(deletePicture({
                    //     id: picture.id,
                    //     AlbumId: picture.AlbumId,
                    // }));
                    navigation.navigate('Gallery');
                } else {
                    // TODO : to implement useForm Hook
                    console.log("ERROR :  ", response);
                }
            })
            .catch((error) => {
                console.log(error);
            });
    }

    const getImage = async () =>{
        // TODO : to delete it
        try {
            const response = await axiosInstance.get('/640x456/?wallpaper,landscape');
            if (response.status === 200) {
                return  response?.request?.responseURL;
            }
        } catch (error) {
            console.error(error);
        }
    }

    // Comment Fields
    const [comment, setComment] = React.useState('');

    useEffect(() => {

        // Get Users to share
        getUsersIds()
            .then((response) => {
                if (response.status === 200) {
                    setUsersToShare([]);
                    response.data.data.forEach((user_temp) => {
                        if (user.id !== user_temp.id) {
                            setUsersToShare((usersToShare) => [...usersToShare, {
                                title: user_temp.firstname,
                                id: user_temp.id,
                            }]);
                        }
                    });
                } else {
                    // TODO : to implement useForm Hook
                    console.log("ERROR :  ", response);
                }
            })
            .catch((error) => {
                console.log(error);
            });

        findPictureBy(picture_id)
            .then((response) => {
                if (response.status === 200) {
                    setPicture(response.data.data);
                    setPictureWatcher(true);
                } else {
                    // TODO : to implement useForm Hook
                    console.log("ERROR :  ", response);
                }
            })
            .catch((error) => {
                console.log(error);
            });

        getUsersByPictureShared(picture_id)
            .then((response) => {
                if (response.status === 200) {
                    const shared_users = response.data.data.map((user_shared) => {
                        return {
                            id: user_shared.user.id,
                            title: user_shared.user.firstname,
                            avatar: user_shared.user.avatar,
                        }
                    });

                    setPicture({
                        ...picture,
                        shared_users
                    });

                    setUsersSelected(shared_users);
                } else {
                    // TODO : to implement useForm Hook
                    console.log("ERROR :  ", response);
                }
            })
            .catch((error) => {
                console.log(error);
            });

        setIsOwner(isAuth && user.id === currentAlbum.UserId);
        setIsLoading(false);

    }, [])

    // TODO  to call API to add comment
    const handlerComment = () => {
        console.log('Comment');
        console.log(comment);
    }

    // TODO : to implement the share
    const handleLike = (likeObject) => {
        console.log("User liked Picture");
        console.log(likeObject)
    }

    const handlerShareSubmit = () => {

        const old_shared_users = picture.shared_users ?? [];
        const new_list = usersSelected ?? [];
        const users_to_delete = old_shared_users.filter(user_temp => !new_list.includes(user_temp));
        const users_to_add = new_list.filter(user_temp => !old_shared_users.includes(user_temp));
        //console.log("Users to delete : ", users_to_delete);
        //console.log("Users to add : ", users_to_add);
        // Check if some users were removed from the list

        if (users_to_add.length > 0 ) {
            sharePicture({
                PictureId: picture.id,
                UsersId: users_to_add.map(user => user.id),
            }).then((response) => {
                if (response.status === 200) {
                    // TODO to show Message
                    //console.log("Picture shared: ");
                } else {
                    // TODO : to implement useForm Hook and Show error message
                    console.log("ERROR :  ", response);
                }
            })
                .catch((error) => {
                    console.log(error);
                });
        }

        if (users_to_delete.length > 0) {
            removeUsersSharedFromPicture({
                PictureId: picture.id,
                UsersId: users_to_delete.map(user => user.id),
            }).then((response) => {
                if (response.status === 200) {
                    console.log("Old users deleted from this Picture: ", picture.id);
                } else {
                    // TODO : to implement useForm Hook and Show error message
                    console.log("ERROR REMOVE USER FROM PICTURE :  ", response);
                }
            })
                .catch((error) => {
                    console.log(error);
                });
        }

        // TODO : To show Success message
        setIsOnShare(false);
    }

    const handlerOnRemoveUserFromSharedList = (user_id) => {
        setUsersSelected(usersSelected.filter(user_temp => user_temp.id !== user_id));
    }

    const handlerOnSelect = (user_id) => {
         const present_user = usersSelected.find(user => user.id === user_id);
         const current_user = usersToShare.find(user => user.id === user_id);
        if (!present_user) {
            setUsersSelected([...usersSelected, current_user]);
        }
    }

    return (
        <View>
            {/* HEADER */}
            <View style={styles.header}>
                <Image
                    style={styles.header_image}
                    source={header_image}/>
                    {/*TODO: to replace by the picture's Picture*/}
                    {/*source={picture.picture}/>*/}
                {/*LIKE ICON*/}

                {
                    !isOwner && <TouchableOpacity
                        style={styles.like}
                        onPress={() => handleLike({
                            picture_id: picture.id,
                            user_id: user.id,
                        })}>
                        <HeartIcon
                            size={24}
                            style={{
                                color: theme.colors.white,
                            }}
                        />
                    </TouchableOpacity>
                }
                {
                    isOwner && <>
                        {/*EDIT ICON*/}
                        <TouchableOpacity
                            style={styles.edit}
                            onPress={() => setIsOnEdit({isOnEdit: !isOnEdit})}>
                            <PencilIcon
                                size={24}
                                style={{
                                    color: theme.colors.white,
                                }}
                            />
                        </TouchableOpacity>

                        {/*EDIT SHARE*/}
                        <TouchableOpacity
                            style={styles.share}
                            onPress={() => setIsOnShare({isOnShare: true})}>
                            <ShareIcon
                                size={24}
                                style={{
                                    color: theme.colors.white,
                                }}
                            />
                        </TouchableOpacity>
                    </>
                }
            </View>

            {/* CONTENT */}
            <View style={styles.content}>
                {/* ALBUM CARD - TITLE */}
                <View>
                    <Text h2 bold default_ style={styles.content.title}>
                        Author : {user.firstname}
                    </Text>
                    <Text small  style={styles.content.subtitle}>
                        Last modification : {(new Date(picture.updatedAt)).toDateString()}
                    </Text>
                    <Text small  style={styles.content.subtitle}>
                        {picture.description}
                    </Text>
                </View>

                {/* ALBUM CARD - SUP INFO */}
                <View style={styles.content.info}>
                    <View style={styles.content.like}>
                        <
                            HeartIcon
                            size={24}
                            style={styles.content.like.icon}
                        />
                        <Text
                            small
                            style={styles.content.like.number}
                        >
                            {picture?.likes?.length}
                        </Text>
                    </View>

                    <View style={styles.content.comment}>
                        <
                            ChatAltIcon
                            size={24}
                            style={styles.content.comment.icon}
                        />
                        <Text
                            small
                            style={styles.content.comment.number}
                        >
                            {picture?.comments?.length}
                        </Text>
                    </View>

                    <View style={styles.content.share}>
                        <
                            ShareIcon
                            size={24}
                            style={styles.content.share.icon}
                        />
                        <Text
                            small
                            style={styles.content.share.number}
                        >
                            {picture.share}
                        </Text>
                    </View>

                </View>

                {/*  PICTURE COMMENT   */}
                <View style={styles.content.comments}>
                    <Text h2 bold color={"default"} style={{
                        marginBottom: theme.sizes.base,
                    }}>
                        Comments
                    </Text>
                    {/*<CommentItems picture_id={picture.id} album_id={picture.AlbumId}/>*/}
                </View>

                {/*    COMMENT INPUT FIELD*/}
                {!isOwner &&

                    <View style={styles.comment}>
                        <Input
                            email
                            label="Comment"
                            placeholder="Write a comment"
                            style={styles.comment.input}
                            onChange={setComment}
                            multiline
                            numberOfLines={4}
                            editable={true}
                        />
                        <Button style={{
                            alignSelf: "flex-end",
                            width: "50%",
                        }} onPress={()=> handlerComment()} variant="outline">
                            Comment
                        </Button>

                    </View>

                }

                {/* On Edit Picture - Modal*/}
                {isOnEdit && <ModalP
                    handleCloseEditModal={()=>{
                        setIsOnEdit(false);
                    }}
                    // picture={picture} // TODO API's uri
                    album_id={album_id}
                    title="Edit Picture"
                >
                    <View style={styles.form}>
                        <Select
                            label="Albums"
                            placeholder="Select an album"
                            data={albumsList}
                            onChange={(album_id)=> {
                                setPictureAlbumId(album_id);
                            }}
                            value={currentAlbum}
                        />

                        <Input
                            label="Description"
                            style={styles.input}
                            onChange={setPictureDescription}
                            placeholder="Enter your album description"
                            value={picture.description}
                        />
                        <Input
                            label="Picture"
                            style={styles.input}
                            onChange={()=>{}}
                            // onChange={setPicturePicture}
                            placeholder="Enter your album picture"
                            value={picture.filename?.toString()}
                        />
                    </View>

                    <Block flex={0.3} row center style={{
                        marginTop: theme.sizes.base * 2,
                    }} >
                        {/* TODO : Refactor */}
                        <Button
                            onPress={()=> handlerEditPicture()}
                            value="primary"
                            style={{
                                position: "relative",
                                top: -20,
                                width: 150,
                                marginRight: theme.sizes.base,
                                padding: theme.sizes.base / 2,
                                backgroundColor: theme.colors.default,
                            }}
                            textColor="white">
                            Save
                        </Button>
                        <Button
                            onPress={()=> handlerDeletePicture()}
                            value="primary"
                            style={{
                                position: "relative",
                                top: -20,
                                width: 150,
                                padding: theme.sizes.base / 2,
                                backgroundColor: theme.colors.danger,
                            }}
                            textColor="white">
                            Delete
                        </Button>
                    </Block>
                </ModalP>
                }

                {/* On Share Picture - Modal*/}
                {isOnShare && <ModalP
                    handleCloseEditModal={()=>{
                        setIsOnShare(false);
                    }}
                    // picture={picture}
                    album_id={picture.AlbumId}
                    title="Share Picture to other users"
                >
                    <View style={styles.form}>
                        <SelectMulti
                            label="Users"
                            placeholder="Select users to share"
                            data={usersToShare}
                            onChange={(user)=> {
                                handlerOnSelect(user);
                            }}
                        />

                    </View>

                    <View style={styles.content.share.user}>
                        <ScrollView>
                            <>
                                {usersSelected.map((user, index)=> {
                                    return <View
                                        style={styles.content.share.user_shared}
                                        key={`${index}-${user.id}`}>
                                        <Text>{user.title}</Text>
                                        <TouchableOpacity
                                            onPress={()=> handlerOnRemoveUserFromSharedList(user.id)}
                                            style={styles.content.share.user_shared.link}
                                            key={`${index}-${user.id}`}>

                                            <TrashIcon
                                                size={theme.sizes.base * 2}
                                                style={styles.content.share.user_shared.icon}
                                            />
                                        </TouchableOpacity>
                                    </View>
                                })}
                            </>
                        </ScrollView>
                    </View>
                    <Block flex={0.3} row center style={{
                        marginTop: theme.sizes.base * 2,
                    }} >
                        {/* TODO : Refactor */}
                        <Button
                            onPress={()=> handlerShareSubmit()}
                            value="primary"
                            style={{
                                position: "relative",
                                top: -20,
                                width: 150,
                                marginRight: theme.sizes.base,
                                padding: theme.sizes.base / 2,
                                backgroundColor: theme.colors.default,
                            }}
                            textColor="white">
                            Share
                        </Button>
                    </Block>
                </ModalP>
                }
            </View>
        </View>
    );
}

export default Picture;

const styles = StyleSheet.create({
    // HEADER
    container: {
        flex: 1,
        //backgroundColor: theme.colors.white,
        alignItems: 'center',
    },
    header: {
        width: '100%',
        height: 280,
        position: 'relative',
    },
    header_image: {
        width: '100%',
        height: 280,
    },
    header_title: {
        position: 'absolute',
        width: '100%',
        height: 250,
        bottom: '-30%',
        textAlign: 'center',
        color: theme.colors.white,
        fontSize: theme.sizes.h1,
        fontWeight: 'bold',
    },
    like: {
        position: 'absolute',
        right: theme.sizes.base,
        top: theme.sizes.base * 2,

        backgroundColor: theme.colors.defaultRGBA(1),
        width: theme.sizes.base * 3,
        height: theme.sizes.base * 3,
        borderRadius: theme.sizes.base * 50,

        alignItems: 'center',
        justifyContent: 'center',
        overflow: 'hidden',
    },
    edit: {
        position: 'absolute',
        bottom: theme.sizes.base,
        left: theme.sizes.base,

        backgroundColor: theme.colors.defaultRGBA(1),
        width: theme.sizes.base * 3,
        height: theme.sizes.base * 3,
        borderRadius: theme.sizes.base * 50,

        alignItems: 'center',
        justifyContent: 'center',
        overflow: 'hidden',
    },
    share: {
        position: 'absolute',
        bottom: theme.sizes.base,
        right: theme.sizes.base,

        backgroundColor: theme.colors.defaultRGBA(1),
        width: theme.sizes.base * 3,
        height: theme.sizes.base * 3,
        borderRadius: theme.sizes.base * 50,

        alignItems: 'center',
        justifyContent: 'center',
        overflow: 'hidden',
    },

    // CONTENT
    content: {
        width: '100%',
        padding: theme.sizes.base,
        //backgroundColor: theme.colors.white,
        subtitle: {
            fontSize: theme.sizes.h1 / 2,
            color: theme.colors.black,
        },
        info: {
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginTop: theme.sizes.base,
        },
        like: {
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: "flex-start",
            icon: {
                fontSize: theme.sizes.h1 / 2,
                color: theme.colors.default,
            },
            number: {
                color: theme.colors.default,
            }
        },
        comment: {
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: "flex-start",
            icon: {
                fontSize: theme.sizes.h1 / 2,
                color: theme.colors.default,
            },
            number: {
                color: theme.colors.black,
            },
        },
        share: {
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: "flex-start",
            icon: {
                fontSize: theme.sizes.h1 / 2,
                color: theme.colors.default,
            },
            number: {
                color: theme.colors.default,
            },
            user: {
                width: '100%',
                height: Dimensions.get('window').height / 3,
                paddingLeft: theme.sizes.base,
                paddingRight: theme.sizes.base,
            },
            user_shared: {
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: "space-between",
                borderStyle: 'solid',
                borderBottomColor: theme.colors.defaultRGBA(0.3),
                borderBottomWidth: 1,
                paddingLeft: theme.sizes.base / 1.5,
                elevation: 1,
                link: {
                    padding: theme.sizes.base / 2,

                    borderStyle: 'solid',
                    borderColor: theme.colors.danger,
                    borderWidth: 1,
                    backgroundColor: theme.colors.danger,
                },
                icon: {
                    color: theme.colors.white,
                }
            }
        },
        comments: {
            height: 250,
        },
    },
    comment_input: {
        backgroundColor: theme.colors.white,
    },
    comment: {
        width: '100%',
        height: theme.sizes.base * 2.1,
        input: {
            width: '100%',
        }
    },
    form : {
        width: '100%',
        padding: theme.sizes.base,
    },
    buttonOpen: {
        backgroundColor: "#F194FF",
    },
    buttonClose: {
        backgroundColor: theme.colors.default,
    },
    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
    },
    modalText: {
        marginBottom: 15,
        textAlign: "center"
    },
    input: {
        width: '100%',
        borderStyle: 'solid',
        borderColor: theme.colors.defaultRGBA(1),
        borderWidth: 1,
    },

});
