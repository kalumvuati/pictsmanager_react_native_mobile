import {View, Text, ScrollView, Image, StyleSheet, TouchableOpacity, Dimensions} from "react-native";
import React, {Component, useEffect, useState} from 'react';
import AlbumCard from "../components/AlbumCard";
import {theme} from "../constants";
import {HeartIcon, PencilAltIcon, PencilIcon, ShareIcon} from "react-native-heroicons/solid";
import PictureItems from "../components/PictureItems";
import {getAlbumBy} from '../store/features/album/AlbumSlice';


const Album = (props) => {
    const [album, setAlbum] = useState(null);
    const [isLoading, setIsLoading] = useState(true);
    const header_image =  require("../../assets/images/BG4.jpg");
    const { navigation, route } = props;
    const { album_id } = route.params;


    useEffect(() => {
        const { album_id } = route.params;
        if (album_id) {
            setAlbum(getAlbumBy(album_id));
        }
    }, []);

    const handleShare = (id) => {
        // TODO : share the album
        alert('Share : ' + id);
    }

    const handleEdit = (id) => {
        // TODO : share the album
        alert('Edit : ' + id);
    }

    const handleLike = (id) => {
        // TODO : share the album
        alert('Liked : ' + id);
    }

    if (isLoading === true && album_id) {
        return (
            <View>
                <Image
                    source={require("../../assets/images/loading/loading1.gif")}
                    style={styles.loading}
                />
            </View>);
    }

    return (
        <ScrollView>

            <View>
                {/* HEADER */}
                <View style={styles.header}>
                    <Image
                        style={styles.header_image}
                        source={album.thumbnail}/>
                    <Text style={styles.header_title}>
                        {album.title} - {album.id}
                    </Text>

                    {/*LIKE ICON*/}
                    <TouchableOpacity
                        style={styles.like}
                        onPress={() => handleLike(album.id)}>
                        <HeartIcon
                            size={24}
                            style={{
                                color: theme.colors.white,
                            }}
                        />
                    </TouchableOpacity>

                    {/*EDIT ICON*/}
                    <TouchableOpacity style={styles.edit}
                        onPress={() => handleEdit(album.id)}>
                        <PencilIcon
                            size={24}
                            style={{
                                color: theme.colors.white,
                            }}
                        />
                    </TouchableOpacity>

                    {/*EDIT SHARE*/}
                    <TouchableOpacity
                        style={styles.share}
                        onPress={() => handleShare(album.id)}>
                        <ShareIcon
                            size={24}
                            style={{
                                color: theme.colors.white,
                            }}
                        />
                    </TouchableOpacity>
                </View>

                {/* CONTENT */}
                <View style={styles.content}>
                    {/* Basic Usage */}
                    <PictureItems album_id={album.id} navigation={navigation}/>

                </View>
            </View>
        </ScrollView>
    );
}

export default Album;

const styles = StyleSheet.create({
    // HEADER
    container: {
        flex: 1,
        backgroundColor: theme.colors.white,
        alignItems: 'center',
    },
    header: {
        width: '100%',
        height: 300,
        position: 'relative',
    },
    header_image: {
        width: '100%',
        height: 300,
    },
    header_title: {
        position: 'absolute',
        width: '100%',
        height: 250,
        bottom: '-30%',
        textAlign: 'center',
        color: theme.colors.white,
        fontSize: theme.sizes.h1,
        fontWeight: 'bold',
    },
    like: {
        position: 'absolute',
        right: theme.sizes.base,
        top: theme.sizes.base * 2,

        backgroundColor: theme.colors.defaultRGBA(1),
        width: theme.sizes.base * 3,
        height: theme.sizes.base * 3,
        borderRadius: theme.sizes.base * 50,

        alignItems: 'center',
        justifyContent: 'center',
        overflow: 'hidden',
    },
    edit: {
        position: 'absolute',
        bottom: theme.sizes.base,
        left: theme.sizes.base,

        backgroundColor: theme.colors.defaultRGBA(1),
        width: theme.sizes.base * 3,
        height: theme.sizes.base * 3,
        borderRadius: theme.sizes.base * 50,

        alignItems: 'center',
        justifyContent: 'center',
        overflow: 'hidden',
    },
    share: {
        position: 'absolute',
        bottom: theme.sizes.base,
        right: theme.sizes.base,

        backgroundColor: theme.colors.defaultRGBA(1),
        width: theme.sizes.base * 3,
        height: theme.sizes.base * 3,
        borderRadius: theme.sizes.base * 50,

        alignItems: 'center',
        justifyContent: 'center',
        overflow: 'hidden',
    },

    // CONTENT
    content: {
        width: '100%',
        paddingLeft: theme.sizes.base,
        paddingRight: theme.sizes.base,
        paddingTop: theme.sizes.base,
        backgroundColor: theme.colors.gray4,
    },
    loading: {
        width: 40,
        height: 40,
        alignSelf: 'center',
        marginTop: theme.sizes.base * 5,
    }
});
