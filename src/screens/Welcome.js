import { View, StyleSheet } from 'react-native';
import React, { Component, useEffect } from 'react';
import { Button, Text, Block } from '../components';
import { theme } from '../constants';
import { MaterialIcons } from '@expo/vector-icons';
import {isAuthenticated, alreadyLoggedIn, logout} from "../store/features/auth/AuthSlice";
import {useSelector } from 'react-redux';

const  Welcome = (props) => {

    const { navigation } = props;
    const isLoggedIn =  useSelector(isAuthenticated);

    useEffect(() => {
        if (isLoggedIn) {
            navigation.navigate('Gallery');
        }
    }, []);

    return (
        <Block
        center
        style={
        styles.container
        }>
            <Block h1 white flex={0.5}>
                <Text h1 white bold style={{
                    fontSize: theme.sizes.font * 2.5,
                }}>Picts Manager</Text>
                <MaterialIcons
                name="camera"
                size={120}
                color="white" 
                style={{
                    flex: 1,
                    alignSelf: 'center',
                    marginTop: 60,
                    marginBottom: 60,
                }}
                />
                
                <Block center>
                    <Button
                        onPress={()=> navigation.navigate('Login')}
                        textColor="default"
                        style={{
                            width: theme.sizes.btnWith,
                        }}
                    >
                        Login
                    </Button>

                    <Button
                        onPress={()=> navigation.navigate('Register')}
                        variant="outline"
                        textColor="white"
                        style={{
                            width: theme.sizes.btnWith,
                        }}
                    >
                        Register
                    </Button>
                </Block>
            </Block>
      </Block>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: theme.colors.default,
        justifyContent: 'center',
        alignItems: 'center',
    },
});

export default Welcome;
