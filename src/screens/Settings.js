import React from "react";
import Text from "../components/Text";
import {Image, SafeAreaView, StyleSheet, Dimensions, TouchableOpacity, View} from "react-native";
import {Block} from "../components";
import {theme} from "../constants";
import {ArrowNarrowLeftIcon} from "react-native-heroicons/solid";
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faUser, faHeart, faHome, faArrowRightFromBracket} from '@fortawesome/free-solid-svg-icons'
import {useDispatch} from 'react-redux';
import { logout } from "../store/features/auth/AuthSlice";

const Settings = (props) => {
    const title = 'Settings';
    const avatar = require("../../assets/images/BG1.jpg");
    const { navigation } = props;
    const dispatch = useDispatch();

    const handlerLogout = () => {
        dispatch(logout());
        navigation.navigate('Login');
    }

    return (
        <SafeAreaView
            style={styles.container}
        >
           <View style={{
               position: 'relative',
               flex: 1,
           }}>

               <Image source={avatar} style={{width: 400, height: 190}}/>
               <TouchableOpacity onPress={()=> navigation.goBack()}>
                   <Text style={styles.navBack}>
                       <ArrowNarrowLeftIcon color={theme.colors.white}/>
                   </Text>
               </TouchableOpacity>
                {/* HEADER */}
              <View>
                  <View style={styles.title}>
                      <View style={styles.title.inner}>
                          <Text h1 bold style={styles.title.inner.h1}>{title}</Text>
                          <Text small style={styles.title.inner.paraph}>Full Stack Developer</Text>
                      </View>
                  </View>
              </View>

               {/* BODY */}
               <View style={styles.profile}>
                   <Block flex={0.1} middle column>
                       <Block flex={0.01} middle >
                           {/*TODO : to configure the typeField for email*/}

                           <View style={styles.profile.settings}>
                               <TouchableOpacity
                                   style={styles.profile.settings.item}
                                   onPress={() => {
                                         navigation.navigate('Gallery');
                                   }}
                               >
                                   <FontAwesomeIcon
                                       size={theme.sizes.base * 2}
                                       icon={faHome} style={styles.profile.settings.item.icon}/>
                                   <Text h1 bold style={styles.profile.settings.item.h1}>Gallery</Text>
                               </TouchableOpacity>

                               <TouchableOpacity
                                   onPress={() => {
                                       navigation.navigate('Favorite');
                                   }}
                                   style={styles.profile.settings.item}
                               >
                                   <FontAwesomeIcon
                                       icon={faHeart}
                                       style={styles.profile.settings.item.icon}
                                       size={theme.sizes.base * 2}
                                   />
                                    <Text h1 bold style={styles.profile.settings.item.h1}>FAvorite Pictures</Text>
                               </TouchableOpacity>

                               <TouchableOpacity
                                   onPress={() => {
                                       navigation.navigate('Profile');
                                   }}
                                   style={styles.profile.settings.item}
                               >
                                   <FontAwesomeIcon
                                       size={theme.sizes.base * 2}
                                       icon={faUser} style={styles.profile.settings.item.icon}/>
                                    <Text h1 bold style={styles.profile.settings.item.h1}>Profile</Text>
                               </TouchableOpacity>

                               <TouchableOpacity
                                   onPress={() => handlerLogout()}
                                   style={styles.profile.settings.item}
                               >
                                   <FontAwesomeIcon
                                       size={theme.sizes.base * 2}
                                       icon={faArrowRightFromBracket} style={styles.profile.settings.item.icon}/>
                                    <Text h1 bold style={styles.profile.settings.item.h1}>Log out</Text>
                               </TouchableOpacity>
                           </View>

                       </Block>
                   </Block>
               </View>
           </View>
         </SafeAreaView>
    );
}

const width = Dimensions.get('window').width;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: width,
        backgroundColor: theme.colors.gray,
    },
    navBack: {
        position: 'absolute',
        top: -80,
        left: 10,
        color: theme.colors.default,

        padding: theme.sizes.base / 2,
        borderRadius: theme.sizes.base * 3,
        backgroundColor: theme.colors.default,
    },
    title: {
        position: 'absolute',
        height: 190,
        top: -125,
        left: 0,
        right: 0,
        inner: {
            width: width,
            height: '100%',
            alignItems: 'center',
            color: 'white',
            h1: {
                fontSize: theme.sizes.h1,
                color: theme.colors.white,
                alignItems: 'center',
            },
            paraph: {
                fontSize: theme.sizes.h6,
                color: theme.colors.white,
                alignItems: 'center',
            },
        },
    },
    profile: {
        height: 700,
        width: '100%',
        backgroundColor: theme.colors.gray4,

        settings: {
            height: '100%',
            justifyContent: 'space-between',
            item: {
                flexDirection: 'row',
                alignItems: 'center',
                padding: theme.sizes.base * 2,
                borderStyle: 'solid',
                borderBottomWidth: 1,
                borderColor: theme.colors.white,
                elevation: .2,
                h1: {
                    marginLeft: theme.sizes.base,
                    color: theme.colors.default,
                    fontWeight: 'normal',
                },
                icon: {
                    color: theme.colors.default,
                },
            }
        }
    }
});

export default Settings;
