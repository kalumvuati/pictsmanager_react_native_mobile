import {SafeAreaView, StyleSheet, TouchableHighlight, TouchableOpacity, View} from 'react-native'
import React, {Component, useEffect, useState} from 'react'
import {Block, Button, Input, Divider, Text, CheckBoxInput} from "../components";
import {theme} from "../constants";
import {MaterialIcons} from "@expo/vector-icons";
import {ArrowNarrowLeftIcon} from "react-native-heroicons/solid";
import { useSelector, useDispatch } from 'react-redux';
import {login} from "../services/AuthService";
import {isAuthenticated, loginSuccess, alreadyLoggedIn, logout} from "../store/features/auth/AuthSlice";

const Login = (props) => {

    const [email, setEmail] = useState('john.doe@domain.ext');
    const [password, setPassword] = useState('strong password more than 8 characters');
    const [remember, setRemember] = useState(false);
    const [title, setTitle] = useState('Sign In');
    const { navigation } = props;
    const isLoggedIn =  useSelector(isAuthenticated);

    const dispatch = useDispatch();

    const handlerLogin = () => {
        // TODO  to call API with user credentials

        login(email, password)
        .then((response) => {
            if (response.status === 200) {
                dispatch(loginSuccess(response.data.data));
                navigation.navigate('Main');
            } else {
                dispatch(logout());
            }
        })
        .catch((error) => {
            console.log(error);
        });

    }

    useEffect(() => {
        if (isLoggedIn) {
            navigation.navigate('Main');
        }
    }, []);

    return (
        <Block flex={1} style={styles.container}>

            <TouchableOpacity onPress={()=> navigation.goBack()}>
                <Text style={styles.navBack}>
                    <ArrowNarrowLeftIcon color={theme.colors.white}/>
                </Text>
            </TouchableOpacity>

            <Block flex={0.3} middle column center>
                <Text h1 white bold style={styles.title}>
                    {title}
                </Text>
            </Block>

            <Block flex={0.1} middle column >
                <Block column >
                    {/*TODO : to configure the typeField for email*/}
                    <Input
                        email
                        label="Email"
                        placeholder="Enter your email"
                        style={styles.input}
                        onChange={setEmail}
                    />
                    {/* Create component called Divider */}
                    <Block color={theme.colors.transparent}/>
                    <Input
                        email
                        label="Password"
                        placeholder="Enter your password"
                        secure
                        style={styles.input}
                        onChange={setPassword}
                    />
                    {/* Create component called Divider */}
                    <Block  color={theme.colors.transparent} />

                    <CheckBoxInput onChange={setRemember} label="Remember"/>

                    <Block  color={theme.colors.transparent} />

                    <Block flex={0.3} column center>
                        <Button
                            onPress={ ()=> handlerLogin()}
                            style={{
                                marginTop: theme.sizes.base,
                                marginBottom: theme.sizes.base,
                                width: theme.sizes.btnWith
                            }}
                            textColor="default"
                        >
                            Login
                        </Button>

                        <Button
                            onPress={()=> navigation.navigate('Forgot')}
                            variant="outline"
                        >
                            Forgot Password
                        </Button>

                    </Block>
                </Block>
            </Block>
        </Block>
    );
}

export default Login;

const styles = StyleSheet.create({
    container: {
        backgroundColor: theme.colors.default,
        padding: theme.sizes.base * 2,
        position: 'relative',
    },
    title: {
        fontSize: theme.sizes.font * 2.5,
        color: theme.colors.white,
        fontWeight: 'bold',
    },
    input: {
        borderBottomColor: theme.colors.gray2,
        borderBottomWidth: StyleSheet.hairlineWidth,
        color: theme.colors.gray2,
        fontSize: theme.sizes.font * 1.25,
        backgroundColor: theme.colors.white,
    },
    border: {
        borderStyle: 'solid',
        borderWidth: 1,
        borderColor: theme.colors.gray,
    },
    navBack: {
        position: 'absolute',
        top: 20,
        left: 0,
        color: theme.colors.white,
        borderStyle: 'solid',
        borderWidth: 1,
        borderColor: theme.colors.white,
        padding: theme.sizes.base / 2,
        borderRadius: theme.sizes.base * 3,
    }
});
