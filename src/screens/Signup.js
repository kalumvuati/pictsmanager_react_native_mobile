import {SafeAreaView, StyleSheet, TouchableHighlight, TouchableOpacity, View} from 'react-native'
import React, {Component, useEffect, useState} from 'react'
import {Block, Button, Input, Divider, Text, CheckBoxInput} from "../components";
import {theme} from "../constants";
import {MaterialIcons} from "@expo/vector-icons";
import {ArrowNarrowLeftIcon} from "react-native-heroicons/solid";
import {useSelector} from 'react-redux';
import {isAuthenticated} from "../store/features/auth/AuthSlice";
import {register} from "../services/AuthService";

const Signup = (props) => {

    const title = 'Sign Up';
    const { navigation } = props;
    const isLoggedIn =  useSelector(isAuthenticated);

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [passwordConf, setPasswordConf] = useState('');
    const [firstname, setFirstname] = useState('');

    useEffect(() => {
        if (isLoggedIn) {
            navigation.navigate('Gallery');
        }
    }, []);

    const handlerSignUp = () => {

        // TODO : to check if password and passwordConf match
        // passwordConf,
        register({
            email,
            password,
            firstname
        })
        .then((response) => {
            if (response.status === 201) {
                navigation.navigate('Login');
            }
        })
        .catch((error) => {
            // TODO : to show error message
            console.log(error);
        });
    }

    return (
        <Block flex={1} style={styles.container}>

            <TouchableOpacity onPress={()=> navigation.goBack()}>
                <Text style={styles.navBack}>
                    <ArrowNarrowLeftIcon color={theme.colors.white}/>
                </Text>
            </TouchableOpacity>

            <Block
                flex={0.3}
                middle
                column
                center
            >
                <Text h1 white bold style={styles.title}>
                    {title}
                </Text>
            </Block>
            <Block
                flex={0.1}
                middle
                column
            >
                <Block>
                    {/*//TODO :To use useForm hook  for validation */}
                    <Input
                        label="Firstname"
                        style={styles.input}
                        onChange={setFirstname}
                        placeholder="Enter your firstname"
                        value={firstname}
                    />
                    <Block color={theme.colors.transparent}/>
                    <Input
                        email
                        label="Email"
                        style={styles.input}
                        placeholder="Enter your email"
                        onChange={setEmail}
                        value={email}
                    />
                    {/* Create component called Divider */}
                    <Block color={theme.colors.transparent}/>
                    <Input
                        label="Password"
                        secure
                        style={styles.input}
                        placeholder="Enter your password"
                        onChange={setPassword}
                        value={password}
                    />
                    {/* Create component called Divider */}
                    <Block color={theme.colors.transparent}/>
                    <Input
                        label="Confirm Password"
                        secure
                        style={styles.input}
                        placeholder="Confirm your password"
                        onChange={setPasswordConf}
                        value={passwordConf}
                    />
                    {/* Create component called Divider */}
                    <Block  color={theme.colors.transparent} />

                    <Block  color={theme.colors.transparent} />

                    <Block flex={0.3} column center >
                        {/* TODO : Refactor */}
                        <Button
                            onPress={()=> handlerSignUp()}
                            style={{
                                marginTop: theme.sizes.base,
                                marginBottom: theme.sizes.base,
                                width: theme.sizes.btnWith,
                            }}
                            textColor="default"
                        >
                            Sign up
                        </Button>

                        <Button
                            onPress={()=> navigation.navigate('Welcome')}
                            variant="outline"
                        >
                            Home
                        </Button>

                    </Block>

                </Block>
            </Block>
        </Block>
    );
}

export default Signup;

const styles = StyleSheet.create({
    container: {
        backgroundColor: theme.colors.default,
        padding: theme.sizes.base * 2,
    },
    title: {
        fontSize: theme.sizes.font * 2.5,
        color: theme.colors.white,
        fontWeight: 'bold',
    },
    input: {
        borderBottomColor: theme.colors.gray2,
        borderBottomWidth: StyleSheet.hairlineWidth,
        color: theme.colors.gray2,
        fontSize: theme.sizes.font * 1.25,
        backgroundColor: theme.colors.white,
    },
    border: {
        borderStyle: 'solid',
        borderWidth: 1,
        borderColor: theme.colors.gray,
    },
    navBack: {
        position: 'absolute',
        top: 20,
        left: 0,
        color: theme.colors.white,
        borderStyle: 'solid',
        borderWidth: 1,
        borderColor: theme.colors.white,
        padding: theme.sizes.base / 2,
        borderRadius: theme.sizes.base * 3,
    }
});