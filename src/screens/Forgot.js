import React, {Component} from "react";
import Block from "../components/Block";
import Text from "../components/Text";
import {Button, CheckBoxInput, Input} from "../components";
import {theme} from "../constants";
import {StyleSheet} from "react-native";
import {Divider} from "react-native-elements";
import {resetPassword} from "../services/AuthService";

const Forgot = (props) => {

    const { navigation } = props;
    const [email, setEmail] = React.useState('');
    const [title, setTitle] = React.useState('Forgot Password');

    const handlerSubmit = () => {
        resetPassword(email)
        .then((response) => {
            if (response.status === 200) {
                navigation.navigate('Login');
            }
        })
        .catch((error) => {
            // TODO  : to show error message if email is not valid
            console.log(error);
        });
    }

    return (
        <Block
            flex={1}
            style={styles.container}
        >
            <Block
                flex={0.3}
                middle
                column
                center
            >
                <Text h1 white bold style={styles.title}>
                    {title}
                </Text>
            </Block>
            <Block
                flex={0.1}
                middle
                column
            >
                <Block>
                    <Block  color={theme.colors.transparent} />

                    <Input
                        email
                        label="Email"
                        placeholder="Enter your email"
                        style={styles.input}
                        onChange={setEmail}
                        value={email}
                    />
                    <Divider with={1}/>
                    {/* Create component called Divider */}
                    <Block color={theme.colors.transparent}/>

                    <Block flex={0.3} column center>
                        <Button
                            onPress={()=> handlerSubmit()}
                            style={{
                                marginTop: theme.sizes.base,
                                marginBottom: theme.sizes.base,
                                width: theme.sizes.btnWith
                            }}
                            textColor="default"
                        >
                            Validate
                        </Button>

                        <Button
                            onPress={()=> navigation.navigate('Login')}
                            variant="outline"
                        >
                            Sign In
                        </Button>

                    </Block>
                </Block>
            </Block>
        </Block>
    );
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: theme.colors.default,
        padding: theme.sizes.base * 2,
    },
    title: {
        fontSize: theme.sizes.font * 2.5,
        color: theme.colors.white,
        fontWeight: 'bold',
    },
    input: {
        borderBottomColor: theme.colors.gray2,
        borderBottomWidth: StyleSheet.hairlineWidth,
        color: theme.colors.gray2,
        fontSize: theme.sizes.font * 1.25,
        backgroundColor: theme.colors.white,
    },
    border: {
        borderStyle: 'solid',
        borderWidth: 1,
        borderColor: theme.colors.gray,
    }
});

export default Forgot;