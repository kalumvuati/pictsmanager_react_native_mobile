import React, { useEffect, useState} from "react";
import Text from "../components/Text";
import {Image, Platform, SafeAreaView, StyleSheet, TouchableOpacity, View} from 'react-native';
import {Block, Button, CheckBoxInput, Input} from "../components";
import {theme} from "../constants";
import {ArrowNarrowLeftIcon} from "react-native-heroicons/solid";
import {Divider} from "react-native-elements";
import BottomTabBar from "../components/BottonNavBar";
import { faUser, faHeart, faHome, faCog} from '@fortawesome/free-solid-svg-icons'
import { useSelector, useDispatch } from 'react-redux';
import {getUser, updateUser, logout} from "../store/features/auth/AuthSlice";
import {update, unRegister} from '../services/UserService';
import ModalP from "../components/ModalP";
import Camera from '../components/Camera';
import * as MediaLibrary from 'expo-media-library';
import {SERVER_URI} from '../config/axios_instance';
import axios from 'axios';
import * as ImageManipulator from 'expo-image-manipulator';

const Profile = (props) =>{

    const user = useSelector(getUser);
    const dispatch = useDispatch();

    const title  = 'Profile';
    const BG = require("../../assets/images/avatar2.jpg");
    const profession = 'Web Developer';
    const { navigation } = props;

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [remember, setRemember] = useState(false);
    const [lastname, setLastName] = useState('');
    const [firstname, setFirstName] = useState('');
    const [isOnUnRegister, setIsOnUnRegister] = useState(false);
    const [reason, setReason] = useState('');
    const [confirm, setConfirm] = useState(false);
    const [isOnUpdate, setIsOnUpdate] = useState(false);
    const [lunchCamera, setLunchCamera] = useState(false);
    const [avatar, setAvatar] = useState('');
    const [formData, setFormData] = useState(null);

    useEffect(() => {
        setEmail(user.email);
        setPassword('');
        setRemember(false);
        setLastName(user.lastname);
        setFirstName(user.firstname);
        if (user.avatar) {
            setAvatar(user.avatar);
        } else {
            setAvatar('/uploads/default/bible.png');
        }
        setReason('');
        setConfirm(false);

    }, []);

    const handleDelete = () => {
        unRegister().then((response) => {
            if (response.status === 200) {
                dispatch(logout());
                dispatch(clearBuffer());
                navigation.navigate('Login');
            }
        }).catch((error) => {
            // TODO : to show error message
            console.log(error);
        });
    }

    const handlerEdit = () => {
        update(
            {
                firstname,
                lastname,
            }
        ).then((response) => {
            if (response.status === 202) {
                dispatch(updateUser(response.data.data));
            }
        }).catch((error) => {
            console.log(error);
        });
    }


    const handlerEditAvatar = async (uri) => {
        const file = await ImageManipulator.manipulateAsync(uri, [
            { resize: { width: 480, height: 640} },
        ], { compress: 0.5});

        uri = file.uri;
        const formDt = new FormData();
        const ext = uri.substring(uri.lastIndexOf('.') + 1);
        formDt.append('avatar', {
            uri: uri,
            name: `${firstname}_avatar${Date.now().toString()}.${ext}`,
            type: `image/${ext}`,
        });

        // To await the time of manipulation of the image
        setTimeout(() => {
            axios({
                'method': 'PUT',
                'url': `${SERVER_URI}/users/edit/avatar`,
                'data': formDt,
                'headers': {
                    'Content-Type': 'multipart/form-data',
                    'Accept': 'application/json',
                },
            }).then(response => {
                if (response.status === 202) {
                    dispatch(updateUser(response.data.data));
                    setAvatar(response.data.data.avatar);
                }
            }).catch(error => {
                console.log("error", error);
            })
        }, 1000);
    }

    const formDataFactory = (uri, filename) => {
        const photo = new FormData();
        const ext = uri.substring(uri.lastIndexOf('.') + 1);
        photo.append(
            `${filename}`,
            {
                uri: Platform.OS === 'android' ? uri : uri.replace('file://',''),
                name: filename,
                type: `image/${ext}`
            }
        );

        return photo;
    }

    return (
        <SafeAreaView
            style={{
                flex: 1,
            }}
        >
           <Block>
                <Image
                    source={BG}
                    style={{width: 400, height: 280}}
                />

               <TouchableOpacity onPress={()=> navigation.goBack()}>
                   <Text style={styles.navBack}>
                       <ArrowNarrowLeftIcon color={theme.colors.white}/>
                   </Text>
               </TouchableOpacity>
                {/* HEADER */}
              <View style={styles.position}>
                  <View style={styles.mask}>
                      <TouchableOpacity
                          onPress={
                              ()=> setLunchCamera(true)
                          }
                          style={styles.mask.sub}>
                          <Image
                              source={{uri: `${SERVER_URI}/${avatar}`}}
                              // source={{ uri: `data:image/jpeg;base64,http${SERVER_URI}` + avatar }}
                              style={styles.mask.sub.mark_item}
                          />
                      </TouchableOpacity>
                  </View>
                    <View style={styles.titles}>
                        <Text h1 bold style={styles.titles}>{firstname} - {lastname?.length ? lastname[0].toLocaleUpperCase() + '.': ''}</Text>
                        <Text small style={styles.titles}>{profession}</Text>
                    </View>
              </View>

               {/* BODY */}
               <View style={styles.profile}>
                   <Block flex={0.1} middle column padding={[theme.sizes.base, theme.sizes.base]}>
                       <Block column >
                           {/*TODO : to configure the typeField for email*/}
                           <Input
                               label="Firstname"
                               labelStyle={{
                                   color: theme.colors.black,
                               }}
                               placeholder="Enter your firstname"
                               value={firstname}

                               edit
                               style={styles.input}
                               onChange={setFirstName}
                               onFocus={()=> onFocus}
                           />

                           <Input
                               label="Lastname"
                               labelStyle={{
                                   color: theme.colors.black,
                               }}
                               placeholder="Enter your lastname"
                                value={lastname}
                               edit
                               style={styles.input}
                               onChange={setLastName}
                           />
                           <Input
                               email
                               label="Email"
                               labelStyle={{
                                    color: theme.colors.black,
                               }}
                               placeholder="Enter your email"
                               value={email}
                               style={styles.input}
                               edit={false}
                               onChange={()=>{}} //TODO : to optimize
                           />

                           <Block flex={0.2} column center>
                               <Button
                                   onPress={()=>handlerEdit()}
                                   style={{
                                       width: theme.sizes.btnWith
                                   }}
                                   variant="outline"
                               >Save</Button>

                               <Button onPress={()=> setIsOnUnRegister(true)} variant="danger">
                                   Delete Account
                               </Button>

                           </Block>
                       </Block>
                   </Block>
                   {/* MODAL ON CREATE a NEW ALBUM*/}
                   {isOnUnRegister && <ModalP
                       handleCloseEditModal={()=>{
                           setIsOnUnRegister(false);
                       }}
                       title="Can you tell us the reason to leave us ?"
                   >
                       <View style={styles.create_album.form}>
                           <Input
                               label="Reason"
                               style={styles.create_album.form.input}
                               onChange={setReason}
                               placeholder="Enter your reason"
                               value={reason}
                           />

                           <CheckBoxInput onChange={setConfirm} label="Confirm"/>

                       </View>

                       <Block flex={0.3} row center style={{
                           marginTop: theme.sizes.base * 2,
                       }} >
                           {/* TODO : Refactor */}
                           <Button
                               onPress={()=> handleDelete()}
                               value="primary"
                               style={{
                                   position: "relative",
                                   top: -20,
                                   width: 150,
                                   marginRight: theme.sizes.base,
                                   padding: theme.sizes.base / 2,
                                   backgroundColor: theme.colors.default,
                               }}
                               textColor="white">
                               Confirm
                           </Button>
                       </Block>
                   </ModalP>
                   }
               </View>
           </Block>

            {lunchCamera &&
                <ModalP
                handleCloseEditModal={()=>{
                    setLunchCamera(false);
                }}
                activatePicture={false}
                title={ "Prise de Photo" }
                style={{
                    width: "100%",
                    height: "100%",
                    backgroundColor: "black",
                    padding: theme.sizes.base * 20,
                }}>
                    <Camera
                        navigation={navigation}
                        goBack={()=>{
                            setLunchCamera(false);
                        }}
                        onTakePicture={(data) => {
                                try {
                                    //const formData = formDataFactory(data.uri, 'avatar');
                                    setAvatar(data.uri);
                                    // TODO : have to use then() to update the state of the avatar
                                    handlerEditAvatar(data.uri);
                                    //setFormData(formData);

                                    // Save picture in the phone
                                    MediaLibrary.saveToLibraryAsync(data.uri).
                                    then(() => {
                                        console.log("Photo saved !");
                                    }).catch((error) => {
                                        console.log("ERROR ON SAVE PHOTO : ", error);
                                    });
                                } catch (error) {
                                    console.error("ERROR : Impossible to take picture. try later", error);
                                }
                            setLunchCamera(false);
                        }}
                    />
                </ModalP>
                }
         </SafeAreaView>
    );

}

export default Profile;

const styles = StyleSheet.create({
    container: {
        backgroundColor: theme.colors.gray,
        position: 'relative',
    },
    navBack: {
        position: 'absolute',
        top: -80,
        left: 10,
        color: theme.colors.default,
        borderStyle: 'solid',
        borderWidth: 1,
        borderColor: theme.colors.default,
        padding: theme.sizes.base / 2,
        borderRadius: theme.sizes.base * 3,
        backgroundColor: theme.colors.default,
    },
    position: {
        position: 'absolute',
        top: 45,
        left: '45%',
        transform: [{translateX: -45}],
    },
    mask: {
        width: 150,
        height: 150,
        borderRadius: 1000,
        overflow: 'hidden',

        justifyContent: 'center',

        borderStyle: 'solid',
        borderWidth: 3,
        borderColor: 'white',
        position: 'relative',
        sub: {
            position: 'relative',
            top: 0,
            left: 0,
            width: '100%',
            height: '100%',
            backgroundColor: 'rgba(0,0,0,0.5)',
            mark_item: {
                position: 'absolute',
                width: '100%',
                height: '100%',
                top: 0,
                left: 0,
                // width: '100%',
                // height: '100%',
                borderRadius: 100,
                resizeMode: 'contain',
                overflow: 'hidden',
                // width: '100%',
                // top: 10,
            },
        },
    },
    titles: {
        textAlign: 'center',
        color: 'white',
    },
    profile: {
        height: 700,
        width: '100%',
        backgroundColor: theme.colors.gray4,
    },
    input: {
        borderStyle: 'solid',
        borderWidth: 0,
        borderColor: theme.colors.default,
        fontSize: theme.sizes.font * 1.25,
        borderRadius: 0,
        borderBottomWidth: 3,
        backgroundColor:  theme.colors.defaultRGBA(0.2),
    },
    create_album: {
        form : {
            width: '100%',
            padding: theme.sizes.base,
            input: {
                width: '100%',
                borderStyle: 'solid',
                borderColor: theme.colors.defaultRGBA(1),
                borderWidth: 1,
            },
        },
    }
});
