import React, {useEffect, useState} from "react";
import {
    Image,
    SafeAreaView,
    ScrollView,
    StyleSheet,
    Text,
    View,
    ActivityIndicator,
} from "react-native";
import {Block, Button, Input} from "../components";
import {theme} from "../constants";
import { PhotographIcon} from "react-native-heroicons/solid";
import AlbumCard from "../components/AlbumCard";
import ModalP from "../components/ModalP";
import {findAlbums} from "../services/AlbumService";
import { useDispatch} from 'react-redux';
import {axiosInstance} from '../config/axios_instance';
import {createOrUpdateAlbum, removeAlbum} from "../services/AlbumService";
import { addAlbum, addOrUpdateAlbum, deleteAlbum, addOrUpdatePicture} from "../store/features/album/AlbumSlice";
import {createOrUpdatePicture} from "../services/PictureService";

const Favorite = (props) => {

    const { navigation } = props;
    const dispatch = useDispatch();

    const header_image = require("../../assets/images/BG2.jpg");

    const [albumModalTitle, setAlbumModalTitle] = useState("Create Album");
    const [albums, setAlbums] = useState([]);
    const [isLoading, setIsLoading] = useState(true);
    const [isOnCreateAlbum, setIsOnCreateAlbum] = useState(false);
    const [isOnCreatePicture, setIsOnCreatePicture] = useState(false);

    // Albums
    const [currentAlbum, setCurrentAlbum] = useState({
        name: '',
        description: '',
    });
    const setAlbumName = (name) => {
        setCurrentAlbum({
            ...currentAlbum,
            name
        });
    }
    const setAlbumDescription = (description) => {
        setCurrentAlbum({
            ...currentAlbum,
            description
        });
    }
    // This variable will watch the creation of an album
    const [albumWatcher, setAlbumWatcher] = useState(true);
    const handlerCreateOrUpdateAlbum = () => {

        createOrUpdateAlbum(currentAlbum)
            .then((response) => {
                if (response.status === 201 || response.status === 202) {
                    dispatch(addOrUpdateAlbum(response.data.data));
                    setCurrentAlbum({});
                    setCurrentAlbum({
                        name: '',
                        description: '',
                    });
                    setAlbumWatcher(true);
                } else {
                    // TODO : to implement useForm Hook
                    console.log("ERROR :  ", response);
                }
            })
            .catch((error) => {
                console.warn(error);
            });
        setIsOnCreateAlbum(false);
    }
    const handlerDeleteAlbum = (album_id) => {
        removeAlbum(album_id)
            .then((response) => {
                if (response.status === 200) {
                    dispatch(deleteAlbum(album_id));
                    setAlbumWatcher(true);
                }
            })
            .catch((error) => {
                console.log(error);
            });
        setIsOnCreateAlbum(false);
    }

    useEffect(() => {
        if (albumWatcher) {
            setIsLoading(true);
            findAlbums().then(response => {
                if (response.status === 200) {
                    for (let i = 0; i < response.data.data.length; i++) {
                        //console.log("Albums : ", i, ": ", response.data.data[i]);
                        dispatch(addAlbum(response.data.data[i]));
                    }
                }
                setAlbums([...response.data.data]);
            }).catch(error => {
                console.log("ERROR :  ", error);
            })
            setIsLoading(false);
            setAlbumWatcher(false);
        }

    }, [albumWatcher]);

    // Picture variables
    const [pictureModalTitle, setPictureModalTitle] = useState("Create Picture");
    const [currentPicture, setCurrentPicture] = useState({
        picture: '',
        description: '',
        AlbumId: '',
        //tags: [] //TODO : list of tags
    });
    const setPictureAlbumId = (AlbumId) => {
        setCurrentPicture({
            ...currentPicture,
            AlbumId,
        });
    }
    const setPicturePicture = (picture) => {
        setCurrentPicture({
            ...currentPicture,
            picture
        });
    }
    const setPictureTags = (tag) => {
        setCurrentPicture({
            ...currentPicture,
            tags: [...currentPicture.tags, tag]
        });
    }
    const setPictureDescription = (description) => {
        setCurrentPicture({
            ...currentPicture,
            description,
        });
    }
    const handlerOnCreatePicture = (AlbumId) => {
        setCurrentPicture({
            picture: '',
            description: '',
            AlbumId : '' + AlbumId
        });
        setIsOnCreatePicture(true);
    }
    const handlerCreatePicture = () => {

        createOrUpdatePicture(currentPicture)
            .then((response) => {
                if (response.status === 201 || response.status === 202) {
                    dispatch(addOrUpdatePicture(response.data.data));
                    setCurrentPicture({});
                    setCurrentPicture({
                        picture: '',
                        description: '',
                        AlbumId: '',
                        //tags: []
                    });
                    setAlbumWatcher(true);
                } else {
                    // TODO : to implement useForm Hook
                    console.log("ERROR :  ", response);
                }
            })
            .catch((error) => {
                console.warn(error);
            });
        setIsOnCreatePicture(false);
    }
    const getImage = async () =>{
        // TODO : to delete it
        try {
            const response = await axiosInstance.get('/640x456/?wallpaper,landscape');
            if (response.status === 200) {
                return  response?.request?.responseURL;
            }
        } catch (error) {
            console.error(error);
        }
    }

    // const wait = (timeout) => {
    //     return new Promise(resolve => setTimeout(resolve, timeout));
    // }
    //
    // const onRefresh = useCallback(() => {
    //     setRefreshing(true);
    //     wait(2000).then(
    //         () => setRefreshing(false)
    //     );
    // }, []);

    const getShowcase = () => {
        return (<View style={styles.header}>
            <Image
                style={styles.header_image}
                source={header_image}/>
            <Text style={styles.header_title}>
                Favorite
            </Text>
        </View>);
    }
    const getAlbums = () => {
        const { navigation } = props;

        if (isLoading) {
            return (
                <View style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginTop: 220,
                }}>
                    <ActivityIndicator size="large" color={theme.colors.default} />
                </View>);
        }
        return(
            <View style={ styles.content }>
                {albums.map((item, index) => (
                    <AlbumCard
                        album={item}
                        navigation={navigation}
                        key={index}
                        style={styles.card}
                        onCreatePicture={() => {
                            handlerOnCreatePicture(item.id);
                        }}
                        handlerDeleteAlbum={handlerDeleteAlbum}
                        onUpdateAlbum={() => {
                            setCurrentAlbum(item);
                            setAlbumModalTitle("Update Album");
                            setIsOnCreateAlbum(true);
                        }}
                    />
                ))}
            </View>
        );
    }

    return (
        <SafeAreaView style={{
            flex: 1,
        }}>
            <View >
                {/* HEADER */}
                {getShowcase()}

                {/* BODY - CONTENT */}
                {albums?.length > 0 ?
                    <ScrollView>
                        {/*todo : to refresh list on refesh ScrollView*/}
                        {getAlbums()}
                    </ScrollView>
                    :
                    <View style={styles.content.empty}>
                        <Text>No album found</Text>
                    </View>
                }

                {/* MODAL ON CREATE a NEW ALBUM*/}
                {isOnCreateAlbum && <ModalP
                    handleCloseEditModal={()=>{
                        setIsOnCreateAlbum(false);
                    }}
                    title={ albumModalTitle }// TODO : to change it on edit
                >
                    <View style={styles.create_album.form}>
                        <Input
                            label={"Name"}
                            style={styles.create_album.form.input}
                            onChange={setAlbumName}
                            placeholder="Enter Album Name"
                            value={currentAlbum.name}
                        />
                        {/* TODO : INPUT FOR UPLOAD FILE - IMAGE */}
                        <Input
                            label="Description"
                            style={styles.create_album.form.input}
                            onChange={setAlbumDescription}
                            placeholder="Enter Album Description"
                            value={currentAlbum.description}
                        />
                    </View>

                    <Block flex={0.3} row center style={{
                        marginTop: theme.sizes.base * 2,
                    }} >
                        {/* TODO : Refactor */}
                        <Button
                            onPress={()=> {
                                handlerCreateOrUpdateAlbum();
                            }}
                            value="primary"
                            style={{
                                position: "relative",
                                top: -20,
                                width: 150,
                                marginRight: theme.sizes.base,
                                padding: theme.sizes.base / 2,
                                backgroundColor: theme.colors.default,
                            }}
                            textColor="white">
                            Save
                        </Button>
                    </Block>
                </ModalP>
                }


                {/* MODAL ON CREATE a NEW PICTURE*/}
                {isOnCreatePicture && <ModalP
                    handleCloseEditModal={()=>{
                        setIsOnCreatePicture(false);
                    }}
                    title={pictureModalTitle}
                >
                    <View style={styles.create_album.form}>
                        <Input
                            label="Album Id"
                            style={styles.create_album.form.input}
                            onChange={setPictureAlbumId}
                            placeholder="Enter Album Id"
                            value={currentPicture.AlbumId}
                        />
                        <Input
                            label="Description"
                            style={styles.create_album.form.input}
                            onChange={setPictureDescription}
                            placeholder="Enter description"
                            value={currentPicture.description}
                        />
                        {/* TODO : INPUT FOR UPLOAD FILE - IMAGE */}
                        <Input
                            label="Picture"
                            style={styles.create_album.form.input}
                            onChange={setPicturePicture}
                            placeholder="Enter Picture"
                            value={currentPicture.picture}
                        />
                    </View>

                    <Block flex={0.3} row center style={{
                        marginTop: theme.sizes.base * 2,
                    }} >
                        {/* TODO : Refactor */}
                        <Button
                            onPress={()=> handlerCreatePicture()}
                            value="primary"
                            style={{
                                position: "relative",
                                top: -20,
                                width: 150,
                                marginRight: theme.sizes.base,
                                padding: theme.sizes.base / 2,
                                backgroundColor: theme.colors.default,
                            }}
                            textColor="white">
                            Save
                        </Button>
                    </Block>
                </ModalP>
                }
            </View>

        </SafeAreaView>
    );
}

export default Favorite;

const styles = StyleSheet.create({
    container: {
        backgroundColor: theme.colors.white,
        alignItems: 'center',
        zIndex: 1,
    },
    header: {
        width: '100%',
        height: 250,
        position: 'relative',
    },
    header_image: {
        width: '100%',
        height: 250,
    },
    header_title: {
        position: 'absolute',
        width: '100%',
        height: 250,
        bottom: '-50%',
        textAlign: 'center',
        color: theme.colors.white,
        fontSize: theme.sizes.h1,
        fontWeight: 'bold',
    },
    content: {
        width: '100%',
        paddingLeft: theme.sizes.base,
        paddingRight: theme.sizes.base,
        paddingTop: theme.sizes.base,
        //backgroundColor: theme.colors.gray4,
        paddingBottom: theme.sizes.base * 15,

        empty: {
            padding: theme.sizes.base * 2,
            justifyContent: 'center',
            alignItems: 'center',
        }
    },
    loading: {
        width: 40,
        height: 40,
        alignSelf: 'center',
        marginTop: theme.sizes.base * 5,
    },
    fixed: {
        button: {
            position: 'fixed',
            color: theme.colors.default,
        }
    },
    button: {
        inner: {
            icon: {
                color: theme.colors.white,
                marginTop: theme.sizes.base / 2,
            }
        }
    },
    create_album: {
        form : {
            width: '100%',
            padding: theme.sizes.base,
            input: {
                width: '100%',
                borderStyle: 'solid',
                borderColor: theme.colors.defaultRGBA(1),
                borderWidth: 1,
            },
        },
    }
});
