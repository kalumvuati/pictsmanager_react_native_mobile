import {post, get, remove, put} from '../config/axios_instance';

export const findAlbums = async () =>  {
    try {
        return await get('/albums', {
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
            }
        });
    } catch (error) {
        return error;
    }
}

export const createAlbum = async (album) =>  {
    try {
        return await post('/albums', album);
    } catch (error) {
        return error.response.data;
    }
}

export const createOrUpdateAlbum = async (album) =>  {
    try {
        if (album.id) {
            return await put('/albums/' + album.id, {
                name: album.name,
                description: album.description,
            });
        } else {
            return await createAlbum(album);
        }
    } catch (error) {
        return error.response.data;
    }
}

export const updateAlbum = async (album) => {
    try {
        return await put(`/albums/${album.id}`, album);
    } catch (error) {
        return error.response.data;
    }
}

export const removeAlbum = async (album_id) =>  {
    try {
        return await remove(`/albums/${album_id}`);
    } catch (error) {
        return error.response.data;
    }
}
