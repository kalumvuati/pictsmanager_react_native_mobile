import {post, get, remove, put} from '../config/axios_instance';

export const findPictureBy = async (picture_id) =>  {
    try {
        return await get(`/pictures/${picture_id}`);
    } catch (error) {
        return error.response.data;
    }
}

export const getUsersByPictureShared = async (picture_id) =>  {
    try {
        return await get(`/users/picture/shares/${picture_id}`);
    } catch (error) {
        return error.response.data;
    }
}

export const removeUsersSharedFromPicture = async (data) =>  {
    try {
        return await remove('/sharepictures', {
            data
        });
    } catch (error) {

        return error.response.data;
    }
}

export const createPicture = async (picture) =>  {
    try {
        return await post('/pictures', picture);
    } catch (error) {
        return error.response.data;
    }
}

export const createOrUpdatePicture = async (picture) =>  {
    try {
        if (picture.id) {
            return await put('/pictures/' + picture.id, {
                name: picture.name,
                description: picture.description,
            });
        } else {
            return await createPicture(picture);
        }
    } catch (error) {
        return error.response.data;
    }
}

export const updatePicture = async (picture) => {
    try {
        return await put(`/pictures/${picture.id}`, picture);
    } catch (error) {
        return error.response.data;
    }
}

export const removePicture = async (picture_id) =>  {
    try {
        return await remove(`/pictures/${picture_id}`);
    } catch (error) {
        return error.response.data;
    }
}

/**
 *
 * @param data : '{PictureId: 1,UsersId: [1,2,3 ] }'
 */
export const sharePicture = async (data) =>  {
    try {
        return await post('/sharepictures', data);
    } catch (error) {
        return error.response.data;
    }
}
