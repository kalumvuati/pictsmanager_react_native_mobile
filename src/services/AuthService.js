import {post} from '../config/axios_instance';

export const login = async (email, password) =>  {
    try {
        return await post('/login', {email, password});
    } catch (error) {
        return error.response.data;
    }
}

export const register = async (user) =>  {
    try {
        return await post('/register', user);
    } catch (error) {
        return error.response.data;
    }
}

export const resetPassword = async (email) =>  {
    try {
        return await post('/reset', {email});
    } catch (error) {
        return error.response.data;
    }
}

export const logoutApi = async (email, password) =>  {
    try {
        return await post('/logout');
    } catch (error) {
        return error.response.data;
    }
}
