import {axiosInstance, post, put, remove, get} from '../config/axios_instance';

export const getUsersIds = async () =>  {
    try {
        return await get('/users/ids');
    } catch (error) {
        return error.response.data;
    }
}

export const update = async (user_data) =>  {
    try {
        console.log("updaload simple : ", user_data);
        return await put('/users/edit', user_data);
    } catch (error) {
        console.log("PROBLEM : ", error);
        return error.response.data;
    }
}

export const unRegister = async () =>  {
    try {
        return await remove('/unRegister');
    } catch (error) {
        return error.response.data;
    }
}


