import React, { Component } from 'react'
import { NavigationContainer } from '@react-navigation/native';

import Welcome from '../screens/Welcome';
import Login from '../screens/Login';
import Register from '../screens/Signup';
import Forgot from '../screens/Forgot';

import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Profile from "../screens/Profile";
import Gallery from "../screens/Gallery";
import Album from "../screens/Album";
import Picture from "../screens/Picture";
import Favorite from "../screens/Favorite";
import Settings from "../screens/Settings";
import {Main} from '../screens/Main';

const Stack = createNativeStackNavigator();

export default class Navigation extends Component {
  render() {
    return (
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen
                    options={{headerShown: false}}
                    name="Welcome"
                    component={Welcome}
                />
                <Stack.Screen
                    options={{headerShown: false}}
                    name="Login"
                    component={Login}
                />
                <Stack.Screen
                    options={{headerShown: false}}
                    name="Register"
                    component={Register}
                />
                <Stack.Screen
                    options={{headerShown: false}}
                    name="Forgot"
                    component={Forgot}
                />

                <Stack.Screen
                    options={{headerShown: false}}
                    name="Profile"
                    component={Profile}
                />
                <Stack.Screen
                    options={{headerShown: false}}
                    name="Gallery"
                    component={Gallery}
                />
                <Stack.Screen
                    options={{ headerShown: false}}
                    name="Album"
                    component={Album}
                />
                <Stack.Screen
                    options={{ headerShown: false}}
                    name="Picture"
                    component={Picture}
                />
                <Stack.Screen
                    options={{ headerShown: false}}
                    name="Favorite"
                    component={Favorite}
                />
                <Stack.Screen
                    options={{ headerShown: false}}
                    name="Settings"
                    component={Settings}
                />
                <Stack.Screen
                    options={{ headerShown: false}}
                    name="Main"
                    component={Main}
                />
            </Stack.Navigator>
        </NavigationContainer>
    )
  }
}
