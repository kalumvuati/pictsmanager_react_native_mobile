import {Component} from "react";
import {Text, View, StyleSheet} from "react-native";
import {theme} from "../constants";

export default class Row extends Component {

    render() {
        const {
            item,
        } = this.props;

        return (
            <View>
                {this.props.children}
            </View>
        );
    }
}
