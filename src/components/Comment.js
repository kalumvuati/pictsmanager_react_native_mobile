import {Component} from "react";
import {Text, View, StyleSheet, Image} from "react-native";
import {theme} from "../constants";

export default class Comment extends Component {
    constructor(props) {
        super(props);
        this.state = {
            comment_image: require("../../assets/images/BG1.jpg"),
        }
    }

    render() {
        const {comment} = this.props;
        return(
            <View style={styles.comment}>
                <View style={styles.comment_left}>
                    <View style={styles.comment_left_top}>
                        <Image
                            source={this.state.comment_image}
                            style={styles.comment_left_top_image}/>
                    </View>
                    <View style={styles.comment_left_bottom}>
                        <Text style={styles.comment_left_bottom_date}>{comment.date}</Text>
                    </View>
                </View>
                <View style={styles.comment_right}>
                    <Text>{comment.comment}</Text>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    comment: {
        flexDirection: "row",
        alignItems: "flex-start",
        marginBottom: theme.sizes.base / 2,
    },
    comment_left: {
        width: "20%",
        alignItems: "center",
    },
    comment_left_top: {
        width: 40,
        height: 40,
        borderRadius: 50,

        overflow: "hidden",
        alignItems: "center",
        justifyContent: "center",
    },
    comment_left_top_image: {
        width: "100%",
        height: "100%",
    },
    comment_left_bottom: {
    },
    comment_left_bottom_date: {
        fontSize: theme.sizes.base * 0.7,
    },
    comment_right: {
        width: "80%",
        marginLeft: theme.sizes.base / 5,
        padding: theme.sizes.base,
        backgroundColor: theme.colors.defaultRGBA(0.3),
        borderRadius: theme.sizes.base / 3,
    }
});
