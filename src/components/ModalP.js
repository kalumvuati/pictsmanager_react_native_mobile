import {Alert, Modal, Pressable, Text, View, StyleSheet, Image, TouchableOpacity, Dimensions} from "react-native";
import {Component} from "react";
import {theme} from "../constants";
import Input from "./Input";
import {Block, Button} from "./index";
import CrossFadeIcon from "react-native-paper/src/components/CrossFadeIcon";
import {
    ArrowRightIcon,
    BackspaceIcon,
    HeartIcon,
    LockOpenIcon,
    PencilIcon,
    PlusIcon, ShareIcon
} from "react-native-heroicons/solid";
import Select from "./Select";
import {getAlbumsSlog, updatePicture} from "../store/data";

export default class ModalP extends Component {

    constructor(props) {
        super(props);
        this.state = {
            picture: require("../../assets/images/BG1.jpg"),
            isLoading: false,
            modalVisible: true,
            activatePicture: this.props.activatePicture === undefined ? true : this.props.activatePicture,
        };
    }

    componentDidMount() {
        const {modal_status, picture} = this.props;
        if (modal_status !== undefined) {
            this.setState({modalVisible: modal_status});
        }

        if (picture !== undefined) {
            this.setState({picture: picture.picture});
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        // Check if the props have changed
        if (prevState.modalVisible !== this.state.modalVisible) {
            this.props.handleCloseEditModal();
        }
    }

    onChange = (input, value) => {
        this.setState({[input.toLowerCase()]: value})
    }

    render() {
        const {children, title } = this.props;
        return (
            <View style={styles.picture}>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.modalVisible}
                    onRequestClose={() => {
                        this.setState({modalVisible: !this.state.modalVisible});
                    }}
                    style={{
                        flex: 1,
                        justifyContent: "center",
                        alignItems: "center",
                        backgroundColor: "rgba(0,0,0,0.5)"
                    }}
                >
                    <View style={styles.centeredView}>
                        <View style={styles.modalView}>

                            {/* HEADER */}
                            { !! this.state.activatePicture && <View style={styles.header}>
                                <Image
                                    style={styles.header_image}
                                    source={this.state.picture}/>
                                <Text style={styles.header.title}>{title}</Text>
                            </View>}

                            <Pressable
                                style={[styles.button, styles.buttonClose]}
                                onPress={() => this.setState({modalVisible: !this.state.modalVisible})}>
                                <Text style={styles.textStyle}>
                                    <PlusIcon
                                        size={24}
                                        style={{
                                            color: theme.colors.white,
                                            transform: [{rotate: "45deg"}]
                                        }}
                                    />
                                </Text>
                            </Pressable>
                            {children}
                        </View>

                    </View>
                </Modal>
            </View>
        );
    }
}

const width = Dimensions.get('window').width;
const styles = StyleSheet.create({
    header: {
        width: '100%',
        height: 280,
        position: 'relative',
        title: {
            position: 'absolute',

            alignSelf: 'center',
            top: '50%',
            transform: [{translateY: -30}],
            padding: theme.sizes.base / 2,

            color: theme.colors.white,
            fontSize: theme.sizes.base * 1.5,
            textAlign: 'center',
            fontWeight: 'bold',
            backgroundColor: theme.colors.whiteAlpha(0.3),
            borderRadius: theme.sizes.base / 2,
        }
    },
    header_image: {
        width: '100%',
        height: 280,
    },
    picture: {
        width: "100%",
    },
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22,
    },
    modalView: {
        position: "relative",
        width: "100%",
        height: "100%",
        margin: 20,
        overflow: "hidden",
        backgroundColor: theme.colors.white,
        borderTopLeftRadius: theme.sizes.base,
        borderTopRightRadius: theme.sizes.base,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5,

    },
    button: {
        position: "absolute",
        right: theme.sizes.base,
        top: theme.sizes.base,
        borderRadius: 20,
        padding: 10,
        elevation: 2,

        backgroundColor: theme.colors.default,
    },
});
