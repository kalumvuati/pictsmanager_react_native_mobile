import React, { Component } from "react";
import { StyleSheet, TextInput } from "react-native";
import * as Icon from "@expo/vector-icons";

import { theme } from "../constants";
import Button from "./Button";
import Block from "./Block";
import Text from "./Text";
import {AntDesign} from "@expo/vector-icons";

export default class Input extends Component {
    state = {
        toggleSecure: false,
        toggleEdit: false
    };

    renderLabel() {
        const { label, error, labelStyle } = this.props;

        return (
            <Block flex={false}>
                {label ? (
                    <Text gray2={!error} accent={error} style={{
                        ...labelStyle,
                        marginBottom: 0,
                        position: 'relative',
                        top: -theme.sizes.base,
                    }}>
                        {label}
                    </Text>
                ) : null}
            </Block>
        );
    }

    renderToggle() {
        const { secure, edit, rightLabel } = this.props;
        const { toggleSecure, toggleEdit } = this.state;

        if (secure) {
            return (
                <Button
                    style={styles.toggle}
                    onPress={() => this.setState({ toggleSecure: !toggleSecure })}
                >
                    {rightLabel ? (
                        rightLabel
                    ) : (
                        <Icon.Ionicons
                            color={theme.colors.gray}
                            size={theme.sizes.font * 1.35}
                            name={!toggleSecure ? "md-eye" : "md-eye-off"}
                        />
                    )}
                </Button>
            );
        } else if(edit) {
            return (
                <Button
                    style={styles.toggle}
                    onPress={() => this.setState({ toggleEdit: !toggleEdit })}
                >
                    {rightLabel ? (
                        rightLabel
                    ) : (
                        <Icon.MaterialIcons
                            color={theme.colors.gray}
                            size={theme.sizes.font * 2}
                            name={!toggleEdit ? "edit" : "edit-off"}
                        />
                    )}
                </Button>
            );
        }
        return null;
    }

    renderRight() {
        const { rightLabel, rightStyle, onRightPress } = this.props;

        if (!rightLabel) return null;

        return (
            <Button
                style={[styles.toggle, rightStyle]}
                onPress={() => onRightPress && onRightPress()}
            >
                {rightLabel}
            </Button>
        );
    }

    render() {
        const {
            email,
            phone,
            number,
            value,
            secure,
            edit,
            keyboardType,
            error,
            style,
            onChange,
            onFocus,
            ...props
        } = this.props;
        const { toggleSecure } = this.state;
        const isSecure = toggleSecure ? false : secure;
        const isEdit = edit ? edit : false;

        const inputStyles = [
            styles.input,
            error && { borderColor: theme.colors.accent },
            style
        ];

        const inputType = email
            ? "email-address"
            : number
                ? "numeric"
                : phone
                    ? "phone-pad"
                    : 'default';
        return (
            <Block flex={false} margin={[theme.sizes.base, 0]}>
                {this.renderLabel()}
                <TextInput
                    style={inputStyles}
                    secureTextEntry={isSecure}
                    autoComplete="off"
                    autoCapitalize="none"
                    autoCorrect={false}
                    keyboardType="default"
                    value={value}
                    {...props}
                    onChange={(e) => onChange(e.nativeEvent.text, this.props.label)}
                    onFocus={() => this.setState({ toggleEdit: false })}
                />
                {this.renderToggle()}
                {this.renderRight()}
            </Block>
        );
    }
}

const styles = StyleSheet.create({
    input: {
        borderWidth: StyleSheet.hairlineWidth,
        borderColor: theme.colors.black,
        borderRadius: theme.sizes.radius ,
        fontSize: theme.sizes.font,
        fontWeight: "500",
        color: theme.colors.black,
        height: theme.sizes.base * 3,
        paddingLeft: 10,
        paddingRight: 10,
        marginTop: -15,
    },
    toggle: {
        position: "absolute",
        alignItems: "flex-end",
        width: theme.sizes.base * 2,
        height: theme.sizes.base * 2,
        top: "30%",
        transform: [{ translateY: -18 }],
        right: 10,
        backgroundColor: "transparent"
    }
});
