import {Component} from "react";
import { StyleSheet } from "react-native";
import Block from "./Block";
import Text from "./Text";
import { Checkbox } from 'react-native-paper';
import InputLabel from "react-native-paper/src/components/TextInput/Label/InputLabel";

export default class CheckBoxInput extends Component {

    state = {
        checked: this.props.cheched
    }

    onChange = (label, value) => {
        this.setState({checked: value});
        this.props.onChange(label, value);
    }

    render() {
        const {label} = this.props;
        return (
            <Block flex={false} row center>
                <Checkbox
                    status={this.state.checked ? 'checked' : 'unchecked'}
                    onPress={(e) =>  this.onChange(label, !this.state.checked)}
                />
                <Text white>{label}</Text>
            </Block>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
    },
    checkboxContainer: {
        flexDirection: "row",
        marginBottom: 20,
    },
    checkbox: {
        alignSelf: "center",
    },
    label: {
        margin: 8,
    },
});