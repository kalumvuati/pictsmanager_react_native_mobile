import React, {Component, useEffect, useState} from "react";
import {useSelector, useDispatch} from "react-redux";
import {getAlbumBy} from "../store/features/album/AlbumSlice";

import {
    Image,
    Text,
    View,
    StyleSheet,
    ScrollView,
    TouchableHighlight,
    Platform,
    FlatList,
    TouchableOpacity, Dimensions
} from "react-native";
import {theme} from "../constants";

const PictureItems = (props) => {

    const {navigation, pictures, album_id, route } = props;
    const [isLoading, setIsLoading] = useState(false);

    const renderItems = (item, navigation) => {
        return <TouchableOpacity
            key={item.id}
            onPress={
                () => navigation?.navigate(
                    'Picture',
                    {picture_id: item.id, album_id})}
            style={styles.album.pictures}>
            <Image
                style={styles.album.pictures}
                source={require("../../assets/images/BG1.jpg")}
            />
        </TouchableOpacity>
    }
    const loadItems = (album_id, navigation) => {
        return (
            <FlatList
                data={pictures}
                horizontal={true}
                showsHorizontalScrollIndicator={false}
                renderItem={({ item }) => renderItems(item, navigation)}
            />
        );
    }

    if (isLoading === true && album_id) {
        return (
            <View>
                <Image
                    source={require("../../assets/images/loading/loading1.gif")}
                    style={styles.loading}
                />
            </View>);
    }

    return loadItems( album_id, navigation);
}

export default PictureItems;

const {width} = Dimensions.get("window");

const styles = StyleSheet.create({

    album : {
        pictures : {
            width: width  - 50,
            height: 190,
            resizeMode: 'cover',
        }
    },
});
