import React, {Component} from "react";
import {StyleSheet, Text, TextInput, TouchableOpacity, View} from "react-native";
import {theme} from "../constants";
import {ArrowDownIcon, ChevronDownIcon} from "react-native-heroicons/solid";

export default class Select extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isShown: false,
            currentValue: "",
            position: "top"
        }
    }
    componentDidMount() {
        const {value, position, data} = this.props;

        if (value !== undefined) {
            this.setState({currentValue: value});
        } else {
            this.setState({currentValue: data[0]});
        }

        if (position !== undefined) {
            this.setState({position: position});
        }
    }

    render() {
        const {label, data, placeholder, onChange} = this.props;
        return(
            <View style={styles.select}>
                <Text style={styles.select.label}>{label}</Text>
                <TextInput
                    style={styles.select.input}
                    placeholder={placeholder}
                    value={this.state?.currentValue?.title}
                    onChange={onChange}
                    activeColor={theme.colors.danger}
                    autoCapitalize={'none'}
                    autoCorrect={false}
                    showSoftInputOnFocus={false}
                    autoFocus={false}

                    onPressIn={() => this.setState({isShown: true})}
                    onBlur={() => this.setState({isShown: false})}
                    defaultValue={this.state?.currentValue?.title}
                />
                <View>
                    <ChevronDownIcon
                        size={20}
                        style={styles.select.icon}
                    />
                </View>
                {this.state.isShown &&
                    <View style={styles.select.dropdown}>
                        {data.map((item, index) => {
                            return <TouchableOpacity
                                key={index}
                                style={styles.select.dropdown.item}
                                onPress={() => {
                                    this.setState({currentValue: item});
                                    this.setState({isShown: false});
                                    onChange(item.id);
                                }}
                            >
                                <Text style={styles.select.dropdown.item.text}>{item.title}</Text>
                            </TouchableOpacity>
                        })
                        }
                    </View>
                }
            </View>
        );
    }
}

const styles = StyleSheet.create({
    select: {
        position: 'relative',
        marginBottom: theme.sizes.base,
        label: {
            color: theme.colors.gray,
        },
        icon : {
            position: 'absolute',

            right: theme.sizes.base,
            color: theme.colors.black,
            top: 16,
            transform: [{translateY: -50}],
        },
        input : {
            borderStyle: 'solid',
            borderWidth: 1,
            borderColor: theme.colors.default,
            padding: theme.sizes.base / 2,
            borderRadius: theme.sizes.base / 2,
        },
        dropdown: {
            position: 'absolute',
            bottom: 0, // TODO: fix this with the position passed as props

            backgroundColor: theme.colors.gray4,
            elevation: 5,
            width: '100%',
            zIndex: 1,
            padding: theme.sizes.base / 3,
            item : {
                text: {
                    color: theme.colors.black,
                    backgroundColor: theme.colors.white,
                    padding: theme.sizes.base,

                    borderStyle: 'solid',
                    borderBottomWidth: 1,
                    borderBottomColor: theme.colors.default,
                }
            }
        }
    }
});