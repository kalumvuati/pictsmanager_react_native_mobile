import React from "react";
import {StyleSheet, View, Text, Dimensions, TouchableOpacity} from "react-native";
import {theme} from "../constants";
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import {Divider} from "react-native-elements";

const BottonNavBar = (props) => {
    const [isSelected, setIsSelected] = React.useState(false);
    const {itemsList} = props
    return (
        <View style={styles.container}>
            <View style={styles.container.bar}>
                {itemsList.map((item, index) => {
                    return <Icon
                        key={index}
                        icon={item.icon}
                        label={item.name}
                        route={item.route}
                        status={item.status}
                        navigation={props.navigation}
                    />
                })}
            </View>
        </View>
    );
}

export const Icon = (props) => {
    const selected = props.status ? styles.container.bar.selected : styles.container.bar.unselected;

    return (
        <>
            <Divider with={1}/>
            <TouchableOpacity
                style={selected.item}
                onPress={() => {
                    props.navigation.navigate(props.route);
                }}
            >
                <FontAwesomeIcon icon={ props.icon } style={selected.item.icon}/>
                <Text style={selected.item.text}>{props.label}</Text>
            </TouchableOpacity>
        </>
    );
}
const width = Dimensions.get('window').width;
const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        width: width,

        flex: 1,
        height: 50,
        backgroundColor: 'transparent',
        justifyContent: 'flex-end',
        alignItems: 'center',
        bar: {
            height: 50,
            width: '100%',
            backgroundColor: theme.colors.primary,

            flexDirection: 'row',
            justifyContent: 'space-around',
            alignItems: 'center',
            selected: {
                item : {
                    width: width / 4,
                    height: '100%',
                    backgroundColor: theme.colors.default,
                    alignSelf: 'center',
                    justifyContent: 'center',
                    alignItems: 'center',
                    icon: {
                        color: theme.colors.white,
                    },
                    text: {
                        color: theme.colors.white,
                    },
                }
            },
            unselected: {
                item : {
                    width: width / 4,
                    height: '100%',
                    backgroundColor: theme.colors.defaultRGBA(0.5),
                    alignSelf: 'center',
                    justifyContent: 'center',
                    alignItems: 'center',
                    icon: {
                        color: theme.colors.white,
                    },
                    text: {
                        color: theme.colors.white,
                    },

                }
            }
        }
    },
});

export default BottonNavBar;

