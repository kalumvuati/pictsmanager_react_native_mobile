import {StyleSheet, TouchableOpacity, Dimensions, SafeAreaView, View, Button, Platform} from 'react-native';
import React, {useEffect, useRef, useState} from 'react';
import {theme} from "../constants";
import {Block, Text} from './index';
import { Camera as CameraExpo, CameraType } from 'expo-camera';
import { StatusBar} from 'expo-status-bar';
// TODO :to use this Icon every where
import { Ionicons } from '@expo/vector-icons';
import {shareAsync} from 'expo-sharing';
import * as MediaLibrary from 'expo-media-library';

const Camera = (props) => {

    const header_image =  require("../../assets/images/BG4.jpg");
    const { navigation, route } = props;
    const cameraRef = useRef(null);
    const [hasPermissionLibrary, setHasPermissionLibrary] = useState(null);
    const [type, setType] = useState(CameraType.back);
    const [permission, requestPermission] = CameraExpo.useCameraPermissions();
    const [hasPermission, setHasPermission] = useState(null);

    useEffect(() => {
        (async () => {
            const { status: statusLibrary } = await MediaLibrary.requestPermissionsAsync();
            const cameraPermission = await CameraExpo.requestCameraPermissionsAsync();
            setHasPermission(statusLibrary === 'granted');
            setHasPermissionLibrary(statusLibrary === 'granted');

        })();
    }, []);

    if (permission === undefined) {
        return (
               <Block flex={1} center middle>
                   <Text h1 center bold>
                       Requesting for camera permission...
                   </Text>
                   <TouchableOpacity onPress={() => navigation.goBack()}>
                       <Text h3 center bold primary>
                           Go back
                       </Text>
                   </TouchableOpacity>
               </Block>
        );
    } else if (!hasPermissionLibrary) {
        return (
            <Block flex={1} center middle>
                <Text h1 center bold>
                    Camera permissions are still loading
                </Text>
                <TouchableOpacity onPress={() => navigation.goBack()}>
                    <Text h3 center bold primary>
                        Go back
                    </Text>
                </TouchableOpacity>
            </Block>
        );
    }

    if (!permission.granted) {
        // Camera permissions are not granted yet
        return (
            <View style={styles.container}>
                <Text style={{ textAlign: 'center' }}>
                    We need your permission to show the camera
                </Text>
                <Button onPress={requestPermission} title="grant permission" />
            </View>
        );
    }

    function toggleCameraType() {
        setType((current) => (
            current === CameraType.back ? CameraType.front : CameraType.back
        ));
    }

    const takePicture = async () => {
        const options = {
            quality: 0.5,
            allowsEditing: true,
            aspects: [2, 2],
            base64: true,
            skipProcessing: true,
            exif: false, // ? TODO : to search about this option
            onPictureSaved: (data) => {
                props.onTakePicture(data);
            }
        };

        if (cameraRef) {
            const data =  cameraRef.current.takePictureAsync(options);
        }
    };

    return (
        <View style={styles.container}>
            <CameraExpo style={styles.camera} type={type} ref={cameraRef}>
                <View style={styles.buttonContainer}>
                    <TouchableOpacity
                        style={styles.button}
                        onPress={()=> props.goBack()}>
                        <Text style={styles.text}> Go Back </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.button}
                        onPress={()=>takePicture()}
                    >
                        <View style={styles.take_btn}></View>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.button}
                        onPress={()=> toggleCameraType()}>
                        <Text style={styles.text}>Flip Camera</Text>
                    </TouchableOpacity>

                </View>
                <StatusBar style="auto" />
            </CameraExpo>

        </View>);
}

export default Camera;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        width: '100%',
    },
    camera: {
        flex: 1,
    },
    buttonContainer: {
        flex: 1,
        width: '100%',
        flexDirection: 'row',
        backgroundColor: 'transparent',
        marginBottom: 64,
    },
    button: {
        flex: 1,
        alignSelf: 'flex-end',
        alignItems: 'center',
    },
    text: {
        fontSize: theme.sizes.base,
        fontWeight: 'bold',
        color: 'white',
    },
    take_btn: {
        width: 64,
        height: 64,
        borderRadius: 32,
        backgroundColor: theme.colors.whiteAlpha(0.2),
        borderStyle: 'solid',
        borderWidth: 2,
        borderColor: theme.colors.white,
    },
});
