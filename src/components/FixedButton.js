import {Component} from "react";
import {Dimensions, Pressable, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import {theme} from "../constants";
import {PhotographIcon, PlusCircleIcon, PlusIcon} from "react-native-heroicons/solid";

export default class FixedButton extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isFixed: false,
            position: 'bottom'
        }
    }

    render() {

        const {position, children, title, onPress} = this.props;

        return (
           <View style={styles.container}>
               <View style={styles.button}>
                   <TouchableOpacity
                       onPress={()=>onPress()}
                       style={styles.button.inner}
                   >
                       {children}
                   </TouchableOpacity>
               </View>
           </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'flex-end',
        position: 'absolute',
        alignItems: 'flex-end',
        bottom: 70,
        left: -10,
        width: '100%',
    },
    button : {
        width: 45,
        height: 45,
        right: theme.sizes.base,
        bottom: theme.sizes.base,
        inner: {
            alignItems: 'center',
            borderRadius: theme.sizes.base / 3,
            width: theme.sizes.base * 3,
            height: theme.sizes.base * 3,
            elevation: 2,
            backgroundColor: theme.colors.default,
        }
    }
});
