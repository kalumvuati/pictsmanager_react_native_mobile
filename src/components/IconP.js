import React from "react";
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import {StyleSheet, View, Text, Dimensions, TouchableOpacity} from "react-native";
import {Divider} from "react-native-elements";

const Icon = (props) => {
    const selected = props.status ? styles.container.bar.selected : styles.container.bar.unselected;

    return (
        <>
            <Divider with={1}/>
            <TouchableOpacity
                style={selected.item}
                onPress={() => {
                    props.navigation.navigate(props.route);
                }}
            >
                <FontAwesomeIcon icon={ props.icon } style={selected.item.icon}/>
                <Text style={selected.item.text}>{props.label}</Text>
            </TouchableOpacity>
        </>
    );
}