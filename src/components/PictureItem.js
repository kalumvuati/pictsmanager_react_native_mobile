import {Component} from "react";
import {Image, Text, View, StyleSheet, ScrollView, TouchableHighlight, Platform, FlatList} from "react-native";
import Row from "./Row";
import {theme} from "../constants";

export default class PictureItem extends Component {
    _renderItem({ item, index, separators }) {

        return (<TouchableHighlight
                key={item.key}
                onPress={() => this._onPress(item)}
                onShowUnderlay={separators.highlight}
                onHideUnderlay={separators.unhighlight}>
                <View style={{backgroundColor: 'white'}}>
                    <Text>{item.title}</Text>
                </View>
            </TouchableHighlight>)
    }

    render() {
        const {item} = this.props;

        return (
            <FlatList
                ItemSeparatorComponent={
                    Platform.OS !== 'android' &&
                    (({ highlighted }) => (
                        <View
                            style={[
                                style.separator,
                                highlighted && { marginLeft: 0 }
                            ]}
                        />
                    ))
                }
                data={items}
                renderItem={this._renderItem}
                horizontal={true}
            />
        );
    }

    _onPress(item) {
        console.log(item)
    }
}

const styles = StyleSheet.create({
    app: {
        flex: 1,
        backgroundColor: theme.colors.gray4,
    },
    item: {
        flex: 1,
        height: 160,
        margin: 1,
        backgroundColor: theme.colors.danger,

        borderStyle: "solid",
        borderColor: "blue",
        borderWidth: 1,
    },
    list: {
        flex: 1,

        borderStyle: "solid",
        borderColor: "red",
        borderWidth: 1,
    },
});
