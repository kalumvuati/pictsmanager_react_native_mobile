import {Image, View, StyleSheet, TouchableOpacity, FlatList, Dimensions} from "react-native";
import React, { useEffect} from "react";
import {theme} from "../constants";
import {useSelector} from "react-redux";
import Text from "../components/Text";
import PictureItems from "./PictureItems";
import {getUser} from "../store/features/auth/AuthSlice";
import {
    ArrowRightIcon,
    HeartIcon, PencilIcon, PhotographIcon,
    PlusSmIcon,
    ShareIcon,
    TrashIcon,
} from 'react-native-heroicons/solid';
import {isAuthenticated} from "../store/features/auth/AuthSlice";
import {getAlbumBy, getIsLoading} from "../store/features/album/AlbumSlice";

const AlbumCard = (props) => {

    const header_image = require("../../assets/images/BG1.jpg");

    const isLoggedIn =  useSelector(isAuthenticated);
    const isLoading =  useSelector(getIsLoading);
    const user =  useSelector(getUser);
    const [isOwner, setIsOwner] = React.useState(false);
    const {
        navigation,
        album,
        onCreatePicture,
        handlerDeleteAlbum ,
        onUpdateAlbum,
    } = props;

    useEffect(() => {
        //console.log(album)
        // TODO : get album by id
        // And to check if this User is the owner of this album
        if (user && album) {
            setIsOwner(user.id === album.UserId);
        }
    }, []);

    const handlerLike = (album_id) => {
        // TODO : add like to album on API
        console.log('Album : ' + album_id);
    }

    return (
        <View style={styles.container}>

            {/* ALBUM'S HEADER */}
            <View style={styles.header}>

                <View style={styles.left}>
                    <View>
                        <Image source={header_image} style={styles.left.round}/>
                    </View>
                    <View style={styles.left.title}>
                        <Text h1 bold>{album?.name} - {album?.id}</Text>
                        <Text small>Pictures : {album?.Pictures?.length}</Text>
                    </View>
                </View>

                <View style={styles.right}>
                    <ArrowRightIcon
                        size={24}
                        style={{
                            color: theme.colors.default,
                        }}
                    />
                </View>
            </View>

            {/* ALBUM'S CONTENTS : PICTURES */}
            <PictureItems
                pictures={album.Pictures}
                album_id={album.id}
                navigation={navigation}
            />

            {/* ALBUM's FOOTER */}
            <View style={styles.footer}>

                <View style={styles.like} >
                   <TouchableOpacity
                       onPress={() => handlerLike(album.id)}
                   >
                       <HeartIcon size={20} style={{
                           color: theme.colors.danger,
                       }}/>
                   </TouchableOpacity>
                    <Text style={{
                        color: theme.colors.black,
                    }}>{album?.likes?.length}</Text>
                </View>

                <View style={styles.comment} >
                    <TouchableOpacity>
                        <ShareIcon size={20} style={{
                            color: theme.colors.danger,
                        }}/>
                    </TouchableOpacity>
                    <Text style={{
                        color: theme.colors.black,
                    }}>{album?.shares?.length}</Text>
                </View>

                {isOwner && <View style={styles.footer.actions}>
                    <TouchableOpacity
                        style={styles.footer.actions.create}
                        onPress={()=> onCreatePicture(album.id)}
                    >
                        <PhotographIcon
                            size={theme.sizes.base * 1.5}
                            style={styles.footer.actions.create.icon}
                        />
                    </TouchableOpacity>

                    {/*TODO  to take picture from Camera */}
                    <TouchableOpacity
                        style={styles.footer.actions.delete}
                        onPress={()=> onUpdateAlbum(album.id)}
                    >
                        <PencilIcon
                            size={theme.sizes.base * 1.5}
                            style={styles.footer.actions.update.icon}
                        />
                    </TouchableOpacity>

                    {/*TODO : Je suis Ici*/}
                    <TouchableOpacity
                        style={styles.footer.actions.delete}
                        onPress={()=> handlerDeleteAlbum(album.id)}
                    >
                        <TrashIcon
                            size={theme.sizes.base * 1.5}
                            style={styles.footer.actions.delete.icon}
                        />
                    </TouchableOpacity>
                </View>}
            </View>
        </View>
    );
}

export default AlbumCard;

const styles = StyleSheet.create({
    container: {
        position: 'relative',
        backgroundColor: '#F5FCFF',
        width: '100%',
        height: 320,

        // borderRadius: theme.sizes.radius,
         overflow: 'hidden',
        marginBottom: theme.sizes.base,
    },
    header : {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',

        paddingLeft: theme.sizes.padding / 3,
        paddingRight: theme.sizes.padding / 3,
        paddingTop: theme.sizes.padding / 4,
        paddingBottom: theme.sizes.padding / 4,
    },
    footer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: theme.sizes.padding / 3,
        fontSize: theme.sizes.font * 1.25,
        backgroundColor: theme.colors.white,
        actions : {
            flexDirection: 'row',
            create: {
                icon: {
                    color: theme.colors.success,
                }
            },
            update: {
                marginLeft: theme.sizes.base * 2,
                icon: {
                    color: theme.colors.warning,
                }
            },
            delete: {
                marginLeft: theme.sizes.base * 2,
                icon: {
                    color: theme.colors.danger,
                }
            }
        }
    },
    image_legend: {
        padding: theme.sizes.padding / 3,
        fontSize: theme.sizes.font * 1.25,
        fontWeight: 'bold',
    },
    like: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        width: '13%',
    },
    comment: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        width: '13%',
    },
    loading: {
        width: 40,
        height: 40,
        alignSelf: 'center',
        marginTop: theme.sizes.base * 5,
    },
    left: {
        flexDirection: 'row',
        alignItems: 'center',

        round: {
            width: 40,
            height: 40,
            borderRadius: 25,
            borderWidth: 1,
            overflow: 'hidden',
        },
        title: {
            flexDirection: 'column',
            marginLeft: theme.sizes.base / 2,
        },
    },
    right: {
        flexDirection: 'column',
    }
});
