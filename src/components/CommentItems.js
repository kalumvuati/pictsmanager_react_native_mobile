import React, {Component} from "react";
import {
    Image,
    Text,
    View,
    StyleSheet,
    ScrollView,
    TouchableHighlight,
    Platform,
    FlatList,
    TouchableOpacity, Dimensions
} from "react-native";
import {theme} from "../constants";
import {getCommentsBy} from "../store/data";
import Comment from "./Comment";

export default class CommentItems extends Component {

    constructor(props) {
        super(props);
        this.state = {
            items: [],
            isLoading: true,
        }
    }

    componentDidMount() {
        const { picture_id, album_id } = this.props;

        this.setState({
            items: getCommentsBy(album_id, picture_id),
            isLoading: false
        });
    }

    loadItems(items) {
        return (
            <FlatList
                data={this.state.items}
                horizontal={false}
                showsVerticalScrollIndicator={false}
                renderItem={({ item }) => (
                    <Comment comment={item} />
                )}
            />
        );
    }

    render() {
        const {navigation, album_id} = this.props;
        if (this.state.isLoading === true && album_id) {
            return (
                <View>
                    <Image
                        source={require("../../assets/images/loading/loading1.gif")}
                        style={styles.loading}
                    />
                </View>);
        }

        return this.loadItems(this.state.items, album_id, navigation);
    }
}
const {width} = Dimensions.get("window");

const styles = StyleSheet.create({

    album : {
        pictures : {
            width: width  - 50,
            height: 190,
            resizeMode: 'cover',
        }
    },
});
