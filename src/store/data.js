//
//
// export const user = {
//     id: 1,
//     name: "John Doe",
//     email: "johndoe@gmail.com",
//     password: '',
//     isValidated: true,
//     avatar: require("../../assets/images/avatar/avatar.jpg"),
//     isAuthenticated: true,
// };
//
// export const gallery = [
//     {
//         id: 1,
//         thumbnail: require("../../assets/images/BG1.jpg"),
//         title: "Animals",
//         likes: "12",
//         shares: "6",
//         user_id: user.id,
//         pictures: [
//             {
//                 id: 1,
//                 picture: require("../../assets/images/animals/animal1.jpg"),
//                 title: "Animal 1",
//                 likes: "12",
//                 share: "2",
//                 description: 'description of the picture for the person who is looking at it',
//                 comments: [
//                     {
//                         id: 1,
//                         comment: "This is a comment",
//                         user: "John Doe",
//                         date: "12/12/2018"
//                     },
//                     {
//                         id: 2,
//                         comment: "This is a comment",
//                         user: "John Doe",
//                         date: "12/12/2018"
//                     },
//                     {
//                         id: 3,
//                         comment: "This is a comment",
//                         user: "John Doe",
//                         date: "12/12/2018"
//                     },
//                     {
//                         id: 4,
//                         comment: "This is a comment",
//                         user: "John Doe",
//                         date: "12/12/2018"
//                     },
//                     {
//                         id: 5,
//                         comment: "This is a comment",
//                         user: "John Doe",
//                         date: "12/12/2018"
//                     },
//                     {
//                         id: 6,
//                         comment: "This is a comment",
//                         user: "John Doe",
//                         date: "12/12/2018"
//                     },
//                     {
//                         id: 7,
//                         comment: "This is a comment",
//                         user: "John Doe",
//                         date: "12/12/2018"
//                     }
//                 ]
//             },
//             {
//                 id: 2,
//                 picture: require("../../assets/images/animals/animal2.jpg"),
//                 title: "Animal 2",
//                 likes: "12",
//                 share: "2",
//                 description: 'description of the picture for the person who is looking at it',
//                 comments: [
//                     {
//                         id: 1,
//                         comment: "This is a comment",
//                         user: "John Doe",
//                         date: "12/12/2018"
//                     },
//                     {
//                         id: 2,
//                         comment: "This is a comment",
//                         user: "John Doe",
//                         date: "12/12/2018"
//                     },
//                     {
//                         id: 3,
//                         comment: "This is a comment",
//                         user: "John Doe",
//                         date: "12/12/2018"
//                     }
//                 ]
//             },
//             {
//                 id: 3,
//                 picture: require("../../assets/images/animals/animal3.jpg"),
//                 title: "Animal 3",
//                 likes: "12",
//                 share: "2",
//                 description: 'description of the picture for the person who is looking at it',
//                 comments: [
//                     {
//                         id: 1,
//                         comment: "This is a comment",
//                         user: "John Doe",
//                         date: "12/12/2018"
//                     },
//                     {
//                         id: 2,
//                         comment: "This is a comment",
//                         user: "John Doe",
//                         date: "12/12/2018"
//                     },
//                     {
//                         id: 3,
//                         comment: "This is a comment",
//                         user: "John Doe",
//                         date: "12/12/2018"
//                     }
//                 ]
//             }
//         ],
//     },
//     {
//         id: 2,
//         thumbnail: require("../../assets/images/BG2.jpg"),
//         title: "Family",
//         likes: "12",
//         shares: "4",
//         pictures: [
//             {
//                 id: 1,
//                 picture: require("../../assets/images/families/family1.jpg"),
//                 title: "Family 1",
//                 likes: "12",
//                 share: "2",
//                 description: 'description of the picture for the person who is looking at it',
//                 comments: [
//                     {
//                         id: 1,
//                         comment: "This is a comment",
//                         user: "John Doe",
//                         date: "12/12/2018"
//                     },
//                     {
//                         id: 2,
//                         comment: "This is a comment",
//                         user: "John Doe",
//                         date: "12/12/2018"
//                     },
//                     {
//                         id: 3,
//                         comment: "This is a comment",
//                         user: "John Doe",
//                         date: "12/12/2018"
//                     }
//                 ]
//             },
//             {
//                 id: 2,
//                 picture: require("../../assets/images/families/family2.jpg"),
//                 title: "Family 2",
//                 likes: "12",
//                 share: "2",
//                 description: 'description of the picture for the person who is looking at it',
//                 comments: [
//                     {
//                         id: 1,
//                         comment: "This is a comment",
//                         user: "John Doe",
//                         date: "12/12/2018"
//                     },
//                     {
//                         id: 2,
//                         comment: "This is a comment",
//                         user: "John Doe",
//                         date: "12/12/2018"
//                     },
//                     {
//                         id: 3,
//                         comment: "This is a comment",
//                         user: "John Doe",
//                         date: "12/12/2018"
//                     }
//                 ]
//             },
//             {
//                 id: 3,
//                 picture: require("../../assets/images/families/family3.jpg"),
//                 title: "Family 3",
//                 likes: "12",
//                 share: "2",
//                 description: 'description of the picture for the person who is looking at it',
//                 comments: [
//                     {
//                         id: 1,
//                         comment: "This is a comment",
//                         user: "John Doe",
//                         date: "12/12/2018"
//                     },
//                     {
//                         id: 2,
//                         comment: "This is a comment",
//                         user: "John Doe",
//                         date: "12/12/2018"
//                     },
//                     {
//                         id: 3,
//                         comment: "This is a comment",
//                         user: "John Doe",
//                         date: "12/12/2018"
//                     }
//                 ]
//             }
//         ],
//         user_id: user.id,
//     },
//     {
//         id: 3,
//         thumbnail: require("../../assets/images/BG3.jpg"),
//         title: "People",
//         likes: "12",
//         shares: "10",
//         pictures: [
//             {
//                 id: 1,
//                 picture: require("../../assets/images/persons/person1.jpg"),
//                 title: "Person 1",
//                 likes: "12",
//                 share: "2",
//                 description: 'description of the picture for the person who is looking at it',
//                 comments: [
//                     {
//                         id: 1,
//                         comment: "This is a comment",
//                         user: "John Doe",
//                         date: "12/12/2018"
//                     },
//                     {
//                         id: 2,
//                         comment: "This is a comment",
//                         user: "John Doe",
//                         date: "12/12/2018"
//                     },
//                     {
//                         id: 3,
//                         comment: "This is a comment",
//                         user: "John Doe",
//                         date: "12/12/2018"
//                     }
//                 ]
//             },
//             {
//                 id: 2,
//                 picture: require("../../assets/images/persons/person2.jpg"),
//                 title: "Person 2",
//                 likes: "12",
//                 share: "2",
//                 description: 'description of the picture for the person who is looking at it',
//                 comments: [
//                     {
//                         id: 1,
//                         comment: "This is a comment",
//                         user: "John Doe",
//                         date: "12/12/2018"
//                     },
//                     {
//                         id: 2,
//                         comment: "This is a comment",
//                         user: "John Doe",
//                         date: "12/12/2018"
//                     },
//                     {
//                         id: 3,
//                         comment: "This is a comment",
//                         user: "John Doe",
//                         date: "12/12/2018"
//                     }
//                 ]
//             },
//             {
//                 id: 3,
//                 picture: require("../../assets/images/persons/person3.jpg"),
//                 title: "Person 3",
//                 likes: "12",
//                 share: "2",
//                 description: 'description of the picture for the person 3',
//                 comments: [
//                     {
//                         id: 1,
//                         comment: "This is a comment",
//                         user: "John Doe",
//                         date: "12/12/2018"
//                     },
//                     {
//                         id: 2,
//                         comment: "This is a comment",
//                         user: "John Doe",
//                         date: "12/12/2018"
//                     },
//                     {
//                         id: 3,
//                         comment: "This is a comment",
//                         user: "John Doe",
//                         date: "12/12/2018"
//                     }
//                 ]
//             },
//             {
//                 id: 4,
//                 picture: require("../../assets/images/persons/person4.jpg"),
//                 title: "Person 4",
//                 likes: "12",
//                 share: "2",
//                 description: 'description of the picture for the person who is looking at it',
//                 comments: [
//                     {
//                         id: 1,
//                         comment: "This is a comment",
//                         user: "John Doe",
//                         date: "12/12/2018"
//                     },
//                     {
//                         id: 2,
//                         comment: "This is a comment",
//                         user: "John Doe",
//                         date: "12/12/2018"
//                     },
//                     {
//                         id: 3,
//                         comment: "This is a comment",
//                         user: "John Doe",
//                         date: "12/12/2018"
//                     }
//                 ]
//             }
//         ],
//         user_id: user.id,
//     },
//     {
//         id: 4,
//         thumbnail: require("../../assets/images/BG3.jpg"),
//         title: "Animals",
//         likes: "12",
//         shares: "4",
//         pictures: [
//             {
//                 id: 1,
//                 picture: require("../../assets/images/animals/animal1.jpg"),
//                 title: "Animal 1",
//                 likes: "12",
//                 share: "2",
//                 description: 'description of the picture for the person who is looking at it',
//                 comments: [
//                     {
//                         id: 1,
//                         comment: "This is a comment",
//                         user: "John Doe",
//                         date: "12/12/2018"
//                     },
//                     {
//                         id: 2,
//                         comment: "This is a comment",
//                         user: "John Doe",
//                         date: "12/12/2018"
//                     },
//                     {
//                         id: 3,
//                         comment: "This is a comment",
//                         user: "John Doe",
//                         date: "12/12/2018"
//                     }
//                 ]
//             },
//             {
//                 id: 2,
//                 picture: require("../../assets/images/animals/animal2.jpg"),
//                 title: "Animal 2",
//                 likes: "12",
//                 share: "2",
//                 description: 'description of the picture for the person who is looking at it',
//                 comments: [
//                     {
//                         id: 1,
//                         comment: "This is a comment",
//                         user: "John Doe",
//                         date: "12/12/2018"
//                     },
//                     {
//                         id: 2,
//                         comment: "This is a comment",
//                         user: "John Doe",
//                         date: "12/12/2018"
//                     },
//                     {
//                         id: 3,
//                         comment: "This is a comment",
//                         user: "John Doe",
//                         date: "12/12/2018"
//                     }
//                 ]
//             },
//             {
//                 id: 3,
//                 picture: require("../../assets/images/animals/animal3.jpg"),
//                 title: "Animal 3",
//                 likes: "12",
//                 share: "2",
//                 description: 'description of the picture for the person who is looking at it',
//                 comments: [
//                     {
//                         id: 1,
//                         comment: "This is a comment",
//                         user: "John Doe",
//                         date: "12/12/2018"
//                     },
//                     {
//                         id: 2,
//                         comment: "This is a comment",
//                         user: "John Doe",
//                         date: "12/12/2018"
//                     },
//                     {
//                         id: 3,
//                         comment: "This is a comment",
//                         user: "John Doe",
//                         date: "12/12/2018"
//                     }
//                 ]
//             }
//         ],
//         user_id: 2,
//     },
//     {
//         id: 5,
//         thumbnail: require("../../assets/images/BG3.jpg"),
//         title: "Animals",
//         likes: "12",
//         shares: "4",
//         pictures: [
//             {
//                 id: 1,
//                 picture: require("../../assets/images/animals/animal1.jpg"),
//                 title: "Animal 1",
//                 likes: "12",
//                 share: "2",
//                 description: 'description of the picture for the person who is looking at it',
//                 comments: [
//                     {
//                         id: 1,
//                         comment: "This is a comment",
//                         user: "John Doe",
//                         date: "12/12/2018"
//                     },
//                     {
//                         id: 2,
//                         comment: "This is a comment",
//                         user: "John Doe",
//                         date: "12/12/2018"
//                     },
//                     {
//                         id: 3,
//                         comment: "This is a comment",
//                         user: "John Doe",
//                         date: "12/12/2018"
//                     }
//                 ]
//             },
//             {
//                 id: 2,
//                 picture: require("../../assets/images/animals/animal2.jpg"),
//                 title: "Animal 2",
//                 likes: "12",
//                 share: "2",
//                 description: 'description of the picture for the person who is looking at it',
//                 comments: [
//                     {
//                         id: 1,
//                         comment: "This is a comment",
//                         user: "John Doe",
//                         date: "12/12/2018"
//                     },
//                     {
//                         id: 2,
//                         comment: "This is a comment",
//                         user: "John Doe",
//                         date: "12/12/2018"
//                     },
//                     {
//                         id: 3,
//                         comment: "This is a comment",
//                         user: "John Doe",
//                         date: "12/12/2018"
//                     }
//                 ]
//             },
//             {
//                 id: 3,
//                 picture: require("../../assets/images/animals/animal3.jpg"),
//                 title: "Animal 3",
//                 likes: "12",
//                 share: "2",
//                 description: 'description of the picture for the person who is looking at it',
//                 comments: [
//                     {
//                         id: 1,
//                         comment: "This is a comment",
//                         user: "John Doe",
//                         date: "12/12/2018"
//                     },
//                     {
//                         id: 2,
//                         comment: "This is a comment",
//                         user: "John Doe",
//                         date: "12/12/2018"
//                     },
//                     {
//                         id: 3,
//                         comment: "This is a comment",
//                         user: "John Doe",
//                         date: "12/12/2018"
//                     }
//                 ]
//             }
//         ],
//         user_id: 2,
//     },
//     {
//         id: 6,
//         thumbnail: require("../../assets/images/BG3.jpg"),
//         title: "Animals",
//         likes: "12",
//         shares: "4",
//         pictures: [
//             {
//                 id: 1,
//                 picture: require("../../assets/images/animals/animal1.jpg"),
//                 title: "Animal 1",
//                 likes: "12",
//                 share: "2",
//                 description: 'description of the picture for the person who is looking at it',
//                 comments: [
//                     {
//                         id: 1,
//                         comment: "This is a comment",
//                         user: "John Doe",
//                         date: "12/12/2018"
//                     },
//                     {
//                         id: 2,
//                         comment: "This is a comment",
//                         user: "John Doe",
//                         date: "12/12/2018"
//                     },
//                     {
//                         id: 3,
//                         comment: "This is a comment",
//                         user: "John Doe",
//                         date: "12/12/2018"
//                     }
//                 ]
//             },
//             {
//                 id: 2,
//                 picture: require("../../assets/images/animals/animal2.jpg"),
//                 title: "Animal 2",
//                 likes: "12",
//                 share: "2",
//                 description: 'description of the picture for the person who is looking at it',
//                 comments: [
//                     {
//                         id: 1,
//                         comment: "This is a comment",
//                         user: "John Doe",
//                         date: "12/12/2018"
//                     },
//                     {
//                         id: 2,
//                         comment: "This is a comment",
//                         user: "John Doe",
//                         date: "12/12/2018"
//                     },
//                     {
//                         id: 3,
//                         comment: "This is a comment",
//                         user: "John Doe",
//                         date: "12/12/2018"
//                     }
//                 ]
//             },
//             {
//                 id: 3,
//                 picture: require("../../assets/images/animals/animal3.jpg"),
//                 title: "Animal 3",
//                 likes: "12",
//                 share: "2",
//                 description: 'description of the picture for the person who is looking at it',
//                 comments: [
//                     {
//                         id: 1,
//                         comment: "This is a comment",
//                         user: "John Doe",
//                         date: "12/12/2018"
//                     },
//                     {
//                         id: 2,
//                         comment: "This is a comment",
//                         user: "John Doe",
//                         date: "12/12/2018"
//                     },
//                     {
//                         id: 3,
//                         comment: "This is a comment",
//                         user: "John Doe",
//                         date: "12/12/2018"
//                     }
//                 ]
//             }
//         ],
//         user_id: 3,
//     }
// ]
//
// user.albums = gallery;
//
// export const getAlbumBy = (album_id) => {
//     return gallery.find(album => album.id === album_id);
// }
//
// export const getPicturesBy = (album_id) => {
//     return getAlbumBy(album_id).pictures;
// }
//
// export const getPictureBy = (album_id, picture_id) => {
//     return getPicturesBy(album_id).find(picture => picture.id === picture_id);
// }
//
// export const getCommentsBy = (album_id, picture_id) => {
//     return getPictureBy(album_id, picture_id).comments;
// }
//
// export const getAlbumsSlog = () => {
//     return gallery.map(album => {
//         return {
//             id: album.id,
//             title: album.title,
//         }
//     });
// }
//
// export const updatePicture = (album_id, picture_id, title, description, picture = "") => {
//     console.log(album_id, picture_id, title, description, picture);
//     return;
//     const album = getAlbumBy(album_id);
//     const pictureIndex = album.pictures.findIndex(picture => picture.id === picture_id);
//     album.pictures[pictureIndex] = picture;
// }
//
// // User Functions
// export const getUserBy = () => {
//     return user;
// }
//
// export const isAuthenticated = () => {
//     return user.isAuthenticated;
// }