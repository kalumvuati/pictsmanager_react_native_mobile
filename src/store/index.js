import { configureStore } from '@reduxjs/toolkit'
import authReducer from './features/auth/AuthSlice'
import albumsReducer from './features/album/AlbumSlice'

export const store = configureStore({
    reducer: {
        authReducer,
        albumsReducer
    },
})