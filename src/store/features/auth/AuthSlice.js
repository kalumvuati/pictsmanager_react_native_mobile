import { createSlice } from '@reduxjs/toolkit'
import {logoutApi} from "../../../services/AuthService";

const initialState = {
    isAuthenticated: false,
    user: {},
    token: '',
    error: '',
    loading: false,
}

export const authSlice = createSlice({
    name: 'auth',
    initialState,
    reducers: {
        loginSuccess: (state, action) => {
            state.isAuthenticated = true;
            state.user = action.payload;
            state.token = action.payload.token;
            state.loading = false;
            state.error = '';
        },
        updateUser: (state, action) => {
            state.user = action.payload;
        },
        alreadyLoggedIn: (state, action) => {
            state.loading = false;
            state.user = action.payload;
            state.isAuthenticated = true;
        },
        loginFailure: (state, action) => {
            state.loading = false;
            state.error = action.payload;
        },
        logout: (state, action) => {

            // TODO  to check response status
            logoutApi();
            state.isAuthenticated = false;
            state.user = {};
            state.token = '';
            state.loading = false;
            state.error = '';
        },
        refreshToken: (state, action) => {
            state.token = action.payload;
        }
    },
})

export const {
    loginSuccess,
    loginFailure,
    logout,
    refreshToken,
    alreadyLoggedIn,
    updateUser,
} = authSlice.actions;

export default authSlice.reducer
export const isAuthenticated = (state) => {
    const { isAuthenticated } = state.authReducer;
    return isAuthenticated;
} ;

export const getToken = (state) => {
    const { token } = state.authReducer;
    return token;
}

export const getUser = (state) => {
    const { user } = state.authReducer;
    return user;
}