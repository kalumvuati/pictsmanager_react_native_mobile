import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    albums: [],
    loading: false,
}

export const albumSlice = createSlice({
    name: 'albums',
    initialState,
    reducers: {

        // Albums
        addAlbum: (state, action) => {
            state.albums = [...state.albums, action.payload];
            state.loading = false;
        },
        addOrUpdateAlbum: (state, action) => {
            if (action.payload.id) {
                const index = state.albums.findIndex(album => album.id === action.payload.id);
                state.albums[index] = action.payload;
            } else {
                state.albums = [...state.albums, action.payload];
            }
            state.loading = false;
        },
        putAlbum: (state, action) => {
            state.albums = state.albums.map(album => {
                if (album.id === action.payload.id) {
                    return action.payload; // TODO : to check soon
                }
                return album;
            });

            state.loading = false;
        },
        deleteAlbum: (state, action) => { // by AlbumId
            state.albums = state.albums.filter(album => album.id !== action.payload);
            state.loading = false;
        },
        clearAlbums: (state) => {
            state.albums = [];
            state.loading = false;
        },

        // Pictures
        addPicture: (state, action) => {
            state.albums = state.albums.map(album => {
                if (album.id === action.payload.AlbumId) {
                    album.Pictures = [...album.Pictures, action.payload];
                }
                return album;
            });
            state.loading = false;
        },
        putPicture: (state, action) => {
            state.albums = state.albums.map(album => {
                if (album.id === action.payload.AlbumId) {
                    album.Pictures = album.Pictures.map(picture => {
                        if (picture.id === action.payload.id) {
                            picture = {
                                ...picture,
                                AlbumId: action.payload.AlbumId,
                                description: action.payload.description,
                                picture: action.payload.picture,
                                tags: action.payload.tags,
                            };
                            return picture; // or action.payload
                        }
                        return picture;
                    } );
                }
                return album;
            } );
            state.loading = false;
        },
        addOrUpdatePicture: (state, action) => {

            if (action.payload.id) {
                // update
                const index = state.albums.findIndex(album => album.id === action.payload.AlbumId);
                state.albums[index].Pictures = state.albums[index].Pictures.map(picture => {
                    if (picture.id === action.payload.id) {
                        picture = {
                            ...picture,
                            AlbumId: action.payload.AlbumId,
                            description: action.payload.description,
                            picture: action.payload.picture,
                            tags: action.payload.tags,
                        };
                        return picture; // or action.payload
                    }
                    return picture;
                } );
            } else {
                // add
                state.albums = state.albums.map(album => {
                    if (album.id === action.payload.AlbumId) {
                        album.Pictures = [...album.Pictures, action.payload];
                    }
                    return album;
                } );
            }
            state.loading = false;
        },
        deletePicture: (state, action) => { // by PictureId
            state.albums = state.albums.map(album => {
                if (album.id === action.payload.AlbumId) {
                    album.Pictures = album.Pictures.filter(picture => picture.id !== action.payload.id);
                }
                return album;
            });
            state.loading = false;
        },

    },
})

export const {
    addAlbum,
    deleteAlbum,
    putAlbum,
    clearAlbums,
    addOrUpdateAlbum,

    // Picture
    addPicture,
    putPicture,
    addOrUpdatePicture,
    deletePicture,
} = albumSlice.actions;

export default albumSlice.reducer

export const getAlbums = (state) => {
    const { albums } = state.albumsReducer;
    return albums;
}

export const getAlbumBy = (state, action) => { // by AlbumId
    const albums = getAlbums(state);
    return albums.find(album => album.id === parseInt(action));
}

export const getIsLoading = (state) => {
    const { loading } = state.albumsReducer;
    return loading;
}

export const getAlbumsSlog = (state) => {
    const { albums } = state.albumsReducer;
    return albums.map(album => {
        return {
            id: album.id,
            title: album.name,
            UserId: album.UserId,
        }
    });
}

export const getPicturesBy = (state, action) => {
    return getAlbumBy(state, action)?.Pictures;
}

export const getPictureBy = (state, action) => {
    return getPicturesBy(state, action.album_id)?.find(picture => picture.id === action.picture_id);
}

export const getCommentsBy = (album_id, picture_id) => {
    return getPictureBy(album_id, picture_id)?.comments;
}
